<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AgentsController extends AppController {
	public $uses = array('Agent','City','AgentCity');
	var $components = array('Session','Email','Cookie');

	
	public function login(){
		if($this->Auth->user('id')){
			if(!$this->request->is('ajax')){
				$this->redirect($this->Auth->redirectUrl());
				//$this->redirect('/');
			}else{
				$this->set('id',$this->Auth->user('id'));
			}
		}
		
		if( $this->request->is('post')  && self::_validation() ) {
			
			if(isset($this->request->data['Agent']['remember'])){
				$this->Cookie->write('Agent',array('email'=>$this->request->data['Agent']['email'],'password'=>$this->request->data['Agent']['password']));
			}else {
				$this->Cookie->delete('Agent');
			}
			if($this->Auth->login() && $this->Auth->user('id') ){
				//echo 'pass';
				$this->Session->delete('click_on');
				$this->redirect($this->Auth->redirectUrl());
			}else{
				$this->Flash->error(__('Invalid email or password, try again.'));
			}
		}
	}
  
  public function register(){
		
		
			if(!empty($this->request->data) && self::_validation()){
				$this->request->data['Agent']['passwordurl'] = md5($this->_randomString());
				$realpassword = $this->request->data['Agent']['password'];
				$this->request->data['Agent']['password'] = Security::hash(Configure::read('Security.salt').$this->request->data['Agent']['password']);
				
				$this->request->data['Agent']['country_id'] = $this->request->data['Agent']['country_id'];
				$this->request->data['Agent']['agent_type'] = NULL;
				$this->request->data['Agent']['commission_percent'] = NULL;
				$this->request->data['Agent']['total_commission'] = NULL;
				$this->request->data['Agent']['commission_cashout'] = NULL;
				$this->request->data['City']['City'][] = $this->request->data['Agent']['city_id'];
				$this->request->data['Agent']['created_at']= date('Y-m-d H:i:s');
				$this->request->data['Agent']['status']=2;
				$this->Agent->create();
				
				
				if($this->Agent->saveAll($this->request->data,array('validate'=>false))){ 
					$data = array(
							'api_key' => Configure::read('MainSite.api_key'),
							'api_secret' => Configure::read('MainSite.api_secret'),
							'agent_id' => $this->Agent->id
					);
					//print_r($data);
					$ch = curl_init(Configure::read('MainSite.url').'/api/user/sendAgentRegisterEmail');
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-type: application/json',
					));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($ch);
					
					$this->Flash->success(__('You have successfully registered as an agent with us. Your profile is under review and once approved, you will receive an email with a successfull confirmation of your account after which you will be able to login.'));
					$this->redirect(array('controller'=>'agents','action'=>'login'));
					//echo $response;die;
				}
			}
		
			$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/country/getCountryList');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$countries = array();
			$response_array = json_decode($response,true);
			if($response_array['status']=='success'){
				$countries = $response_array['results'];
			}
			
			$cities = array();
			if(!empty($this->request->data)){
				$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'country_id' => $this->request->data['Agent']['country_id']
				);
				$ch = curl_init(Configure::read('MainSite.url').'/api/country/getCityList');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-type: application/json',
				));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($ch);
				
				
				$response_array = json_decode($response,true);
				
				if($response_array['status']=='success'){
					$cities = $response_array['results'];
				}
				
				unset($this->request->data['Agent']['password']);
				unset($this->request->data['Agent']['confirmpassword']);
			}
			
			$this->set('countries',$countries);
			$this->set('cities',$cities);
	}
	public function loadCity(){
		$this->layout = '';
		$this->response->type('json');
		//print_r($this->request->query);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'country_id' => $this->request->data['country_id'],
					'agent_id' =>  $this->Auth->user('id')
		);
		if($this->Auth->user('id')){
			$ch = curl_init(Configure::read('MainSite.url').'/api/country/getAgentCityList');
		}else{
			$ch = curl_init(Configure::read('MainSite.url').'/api/country/getCityList');
		}
		
		
			
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			$cities = array();
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			//$this->set('status',$response_array['status']);
	}
	
	
	public function loadCourse(){
		$this->layout = '';
		$this->response->type('json');
		//print_r($this->request->query);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'city_id' => $this->request->data['city_id']
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/getCourseList');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			//$this->set('status',$response_array['status']);
	}
	
	
	
	public function loadAddons(){
		$this->layout = '';
		$this->response->type('json');
		//print_r($this->request->query);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'course_id' => $this->request->data['course_id'],
					'city_id' =>$this->request->data['city_id'],
					'pax' =>$this->request->data['pax']
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/getCourseAddons');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			//$this->set('status',$response_array['status']);
	}
	
	public function loadCourseDates(){
		$this->layout = '';
		$this->response->type('json');
		//print_r($this->request->query);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'course_id' => $this->request->data['course_id']
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/getCourseDates');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			//$this->set('status',$response_array['status']);
	}
	
	public function loadCoursePickupDetail(){
		$this->layout = '';
		$this->response->type('json');
		//print_r($this->request->query);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'course_id' => $this->request->data['course_id']
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/getCoursePickupDetail');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			echo $response;die;
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			//$this->set('status',$response_array['status']);
			$this->render("load_course_dates");
	}
	
	public function loadCourseTimming(){
		$this->layout = '';
		$this->response->type('json');
		
		//print_r($this->request->query);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'course_id' => $this->request->data['course_id'],
					'date' =>   $this->request->data['date']
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/getCourseTimmings');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			//$this->set('status',$response_array['status']);
	}
	public function loadCoursePax(){
		$this->layout = '';
		$this->response->type('json');
	
		//print_r($this->request->data);die;
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'course_id' => $this->request->data['course_id'],
					'date' =>   $this->request->data['date'],
					'time' =>   $this->request->data['time'],
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/getCoursePax');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			
			$response_array = json_decode($response,true);
			
			//if($response_array['status']=='success'){
				//$cities = $response_array['results'];
			//}
			
			$this->set('response',$response_array);
			$this->render("load_course_dates");
			//$this->set('status',$response_array['status']);
	}
	
	
	
	public function ajax_validation(){
		self::_validation();
	}
	private function _validation($action=null){
		if($this->request->data['Agent']['form-name']=='AgentRegistrationForm'){
			$this->Agent->setValidation('BA-AgentRegistrationSETS');
		}
		if($this->request->data['Agent']['form-name']=='AgentLoginForm'){
			$this->Agent->setValidation('BA-AgentLoginSETS');
		}
		
		if($this->request->data['Agent']['form-name']=='AgentResetPasswordForm'){
			$this->Agent->setValidation('BA-AgentResetPasswordSETS');
		}
		
		
		$this->Agent->set($this->request->data);
		$result = array();
		if($this->Agent->validates()) {
			$result['error'] = 0;
		}else{
			$result['error'] = 1;
		}
		$result['errors'] = $this->Agent->validationErrors;
		$errors = array();
		foreach($result['errors'] as $field => $data){
			$errors['Agent'.Inflector::camelize($field)] = array_pop($data);
		}
		$result['errors'] = $errors;
		if($result['error']==1){
			$view = new View($this, false);
			$this->Session->setFlash(__('Please fill all required field(s).'),'default','','error');
			$result['error_msg'] =  $view->element('message');
		}
		if($this->request->is('ajax')) {
			$this->autoRender = false;
			echo json_encode($result);
			return;
		}
		return (int)($result['error'])?0:1; 
		 
		  
	}
	public function dashboard(){
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'country_id' =>$this->Auth->user('country_id')
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/country/getCountryList');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$countries = array();
			$response_array = json_decode($response,true);
			if($response_array['status']=='success'){
				$countries = $response_array['results'];
			}
			
			
			
			
			
			$cities = array();
			if(!empty($this->request->data)){
				//echo '<pre>';print_r($this->request->data);die;
				
				$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
				);
				$data = array_merge($data,$this->request->data['Booking'],array('agent_id'=>$this->Auth->user('id')));
				$ch = curl_init(Configure::read('MainSite.url').'/api/course/saveBooking');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-type: application/json',
				));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($ch);
				$response_array = json_decode($response,true);
				
				if($response_array['status']=='success'){
					$this->Flash->success(__('Booking is created successfully.'));
					$this->redirect(array('controller'=>'agents','action'=>'dashboard'));
				}
				
				
				$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'country_id' => $this->request->data['Booking']['country_id']
				);
				$ch = curl_init(Configure::read('MainSite.url').'/api/country/getCityList');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-type: application/json',
				));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($ch);
				
				
				$response_array = json_decode($response,true);
				
				if($response_array['status']=='success'){
					$cities = $response_array['results'];
				}
				
				//unset($this->request->data['Agent']['password']);
				//unset($this->request->data['Agent']['confirmpassword']);
			}
			
			
			
			
		
		
		
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'agent_id' => $this->Auth->user('id')
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/bookings/getAgentProfile');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			$agent_data = array();
			$response_array = json_decode($response,true);
			if($response_array['status']=='success'){
				$agent_data = $response_array['results'];
			}
			
		$bookings = self::_loadfuturebooking($this->Auth->user('id'),0);
		$past_bookings = self::_loadpastbooking($this->Auth->user('id'),0);
		$agent_data['bookings'] = $bookings['results']['bookings'];
		$agent_data['past_bookings'] = $past_bookings['results']['bookings'];
		
		$this->Session->delete('futurebookingmonthloop');
		
		$this->set('countries',$countries);
		$this->set('future_booking_params',$bookings['results']['params']);
		$this->set('past_booking_params',$past_bookings['results']['params']);
		$this->set('cities',$cities);
		$this->set('agent_data',$agent_data);
		$this->set('courses',array());
		$this->set('addons',array());
		$this->set('dates',array());
		$this->set('time',array());
		$this->set('pax',array());
		
		
		//echo 'test';
		//$this->autoRender = false;
	}
	public function logout(){
		$this->redirect($this->Auth->logout());
		return;
	}
	public function calculate(){
		$this->layout = '';
		$this->response->type('json');
		
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'course_id' => $this->request->data['course_id'],
					'date' =>   $this->request->data['date'],
					'time' =>   $this->request->data['time'],
					'pax' =>   $this->request->data['pax'],
					'add_ons' =>   $this->request->data['add_ons'],
					'agent_id' => $this->Auth->user('id')
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/calculateBookingPrice');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			$cities = array();
			
			$response_array = json_decode($response,true);
			
			$this->set('response',$response_array);
			$this->render("load_course_dates");
	}
	

	public function loadfuturebooking(){
		$results = self::_loadfuturebooking($this->Auth->user('id'),$this->request->data['page']);
		$this->layout = '';
		$this->autoRender = false;
		$response = array();
		if($this->request->is('ajax')){
			$view = new View ($this);
			$response['html'] = $view->element('future_booking_loop',array('agent_data'=>$results['results']));
			$response['html'] = preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "\n", $response['html']));
		}
		//pr($results['results']);
		$response['next_page'] = $results['results']['params']['nextPage'];
		$response['status'] = "success";
		echo json_encode($response);
		return;
	}
	
	private function _loadfuturebooking($agent_id = null,$page){
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'agent_id' => $agent_id,
					'page' => $page
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/bookings/agentFutureBookings');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$bookings = array();
			//echo $response;die;
			$response_array = json_decode($response,true);
			if($response_array['status']=='success'){
				$bookings = $response_array['results'];
			}
			
			return array('results'=>$bookings);
	}
	
	public function loadpastbooking(){
		$results = self::_loadpastbooking($this->Auth->user('id'),$this->request->data['page']);
		
		$this->layout = '';
		$this->autoRender = false;
		$response = array();
		if($this->request->is('ajax')){
			$view = new View ($this);
			$response['html'] = $view->element('past_booking_loop',array('agent_data'=>$results['results']));
			$response['html'] = preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "\n", $response['html']));
		}
		//pr($results['results']);
		$response['next_page'] = $results['results']['params']['nextPage'];
		$response['status'] = "success";
		echo json_encode($response);
		return;
	}
	
	public function loadbooking(){
		$this->layout = '';
		$this->response->type('json');
		
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'booking_id' => $this->request->data['booking_id'],
					'agent_id' => $this->Auth->user('id')
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/bookings/loadBooking');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			
			$response_array = json_decode($response,true);
			
			$this->set('response',$response_array);
			$this->render("load_course_dates");
	}
	
	public function updatebooking(){
		$this->layout = '';
		$this->response->type('json');
		
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'booking_id' => $this->request->data['id'],
					'date' => $this->request->data['date'],
					'time' => $this->request->data['time'],
					'agent_id' => $this->Auth->user('id')
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/course/updateBooking');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$cities = array();
			
			$response_array = json_decode($response,true);
			
			$this->set('response',$response_array);
			$this->render("load_course_dates");
	}
	
	private function _loadpastbooking($agent_id = null,$page){
		$data = array(
					'api_key' => Configure::read('MainSite.api_key'),
					'api_secret' => Configure::read('MainSite.api_secret'),
					'agent_id' => $agent_id,
					'page' => $page
			);
			$ch = curl_init(Configure::read('MainSite.url').'/api/bookings/agentPastBookings');
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-type: application/json',
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			$bookings = array();
			//echo $response;die;
			$response_array = json_decode($response,true);
			if($response_array['status']=='success'){
				$bookings = $response_array['results'];
			}
			
			return array('results'=>$bookings);
	}
	
	function resetpassword() {
		if(!empty($this->request->data) && self::_validation()) {
			$user = $this->Agent->find('first',array('conditions'=>array('Agent.email'=>$this->request->data['Agent']['email'])));
			if(!empty($user['Agent']['id'])) {
				unset($user['Agent']['password']);
				
				$user['Agent']['passwordurl'] = Security::hash(Configure::read('Security.salt').$this->RandomString());
				
				$this->Agent->set(array(
										'id'=>$user['Agent']['id'],
										'passwordurl'=>$user['Agent']['passwordurl']										
									));	
				$this->Agent->save();
				$this->__resetpassword_mail($user['Agent']['id']);
				$this->Flash->success(__('Mail with password reset link will be sent to email. Please follow the instructions to reset your password.'));
				$this->redirect(array('controller'=>'agents','action'=>'resetpassword'));
			}
			else {
					if(!empty($this->request->data['Agent']['email']) && empty($user)){
						$this->Flash->success(__('The email address you entered is not registered with us. Please try again using a different email address.'));
						$this->redirect(array('controller'=>'agents','action'=>'resetpassword'));
					}
			}
		}
	}
	
	function RandomString() {
		$characters = '$&@!0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < 15; $i++) {
			$arr1 = str_split($characters);
			$randstring .= $arr1[rand(0, $i)];
		}
		return $randstring;
	}
	
	private function __resetpassword_mail($agent_id){
		
		$data = array(
							'api_key' => Configure::read('MainSite.api_key'),
							'api_secret' => Configure::read('MainSite.api_secret'),
							'agent_id' => $agent_id,
							'base_url' => Router::url('/',true)
					);
					//print_r($data);
					$ch = curl_init(Configure::read('MainSite.url').'/api/user/sendAgentResetEmail');
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-type: application/json',
					));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($ch);
		
		
		
		/*
		$this->loadModel('MailManager.Mail');
		$mail=$this->Mail->read(null,$mail_id);
		$heading=$mail['Mail']['heading'];
		$base_url = Router::url('/', true);
				

		$link = $base_url.'course_manager/users/passwordurl/'.$user['User']['passwordurl']; 
		
		$body=str_replace('{FNAME}',ucfirst($user['User']['first_name']),$mail['Mail']['mail_body']);
		$body=str_replace('{LNAME}',ucfirst($user['User']['last_name']),$body);
		$body=str_replace('{URL}',$link,$body);
		$body=str_replace('{EMAIL}',$user['User']['email'],$body);
		
		
		
        //$email = new CakeEmail();
        parent::_sendMail();
        $email = $this->email;
        
		$email->to($user['User']['email']);
		$email->subject($mail['Mail']['mail_subject']);
		//$email->from($this->setting['site']['site_contact_email']);
		$email->emailFormat('html');
		$email->template('default');
		$email->viewVars(array('data'=>$body,'background_image'=>Configure::read('Site.background_image'),'facebook'=>$this->setting['social']['facebook'],'twitter'=>$this->setting['social']['twitter'],'google_plus'=>$this->setting['social']['google'],'pinterest'=>$this->setting['social']['pinterest'],'logo'=>Configure::read('Site.Email_logo'),'url'=>$this->setting['site']['site_url']));
		$email->send();
		*/
	}
	
	function passwordurl($str=null) {
		$this->loadModel('ContentManager.Page');
		$site_url=Configure::read('Site.url');
		$user_check = $this->Agent->find('first',array('conditions'=>array('Agent.passwordurl'=>$str)));
		
		$user_id=$user_check['Agent']['id'];
		$status = 0;
	 	if(!empty($user_check) && !empty($str)) {
			
			if(!empty($this->request->data) && $this->_validation()) {
				if($this->request->data['Agent']['password']==$this->request->data['Agent']['confirm_password']) {
				$password = $this->request->data['User']['password'];
				$this->Agent->read(null, $user_id);
				$this->Agent->set(
								array(
									'passwordurl' => '',
									'email' => $user_check['Agent']['email'],
									'password' => Security::hash(Configure::read('Security.salt').$password),
									'status' => $user_check['Agent']['status'],
									
								)
						);
					
					$this->Agent->save();
					
					$this->request->data['Agent']['email']=$user_check['Agent']['email'];
					$this->request->data['Agent']['password']=$password;
					$this->Auth->login();
					
					$this->redirect('/');
				
				}else {
					$this->Session->setFlash('New password and confirm password does not match., try again','default','msg','error');
				}
				$status = 1;				
			}			
		}
		else {
			if($status == 0){
				$this->Session->setFlash('Invalid link or password, try again','default','msg','error');
				///$this->redirect(array('controller'=>'users','action'=>'signup','plugin'=>'course_manager'));
			}
			
		}
		$this->request->data = array();
		$this->set('str',$str);
	}
	
	
}
