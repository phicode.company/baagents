<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	 public $components = array('DebugKit.Toolbar','Auth','Flash');
	 
	 function beforeFilter(){
		Configure::load('Config');
		
		AuthComponent::$sessionKey = 'Auth.Agent';
		//	AuthComponent::$redirect = 'Auth.AgentRedirect';
			$this->Auth->loginRedirect = '/agents/dashboard';
			//$this->Auth->loginRedirect = '/';
			$this->Auth->loginAction = Router::url(array('controller'=>'agents','action'=>'login','admin'=>false));
			$this->Auth->logoutRedirect = '/';
			$this->Auth->authError = 'Did you really think you are allowed to see that?';
			$this->Auth->authenticate =  array(
						'Form' => array(
							'userModel'=>'Agent',
							'fields' => array('username' => 'email'),
							'scope' => array('Agent.status' => 1)
							,'authorize' => array('Controller')
							)
						
							);
			$this->Auth->allow();
			$this->Auth->deny('dashboard'/*,'loadCity'*/,'loadCourse','loadAddons','loadCourseDates','loadCourseTimming','loadCoursePax');
		
		
		
	 }
	 
	 protected function _randomString(){
		$characters = '$&@!0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < 15; $i++) {
			$arr1 = str_split($characters);
			$randstring .= $arr1[rand(0, $i)];
		}
		return $randstring;
	}
}
