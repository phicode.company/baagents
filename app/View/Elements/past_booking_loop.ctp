<?php if(!empty($agent_data['past_bookings'])){ ?>
	<?php $current_month = null; ?>
	<?php $header_set = false; ?>
	<?php $counter = 0; ?>
	<?php foreach($agent_data['past_bookings'] as $_booking){ ?>
	<?php if($current_month!=date("n",strtotime($_booking['Booking']['date']))){
		$current_month = date("n",strtotime($_booking['Booking']['date'])); ?>
		
		<table width="100%" cellpadding="0" cellspacing="0" class="<?php echo ($current_month!=date("n"))?'disable':''; ?>">
	
		<?php if(!$this->Session->read('pastbookingmonthloop')){ ?>
		<tr class="pd-bt">
			<td colspan="7"><h4><?php echo date("F",strtotime($_booking['Booking']['date'])); ?></h4></td>
		</tr>
		<?php  } ?>
		<?php SessionComponent::write('pastbookingmonthloop',$current_month); ?>
	<?php } ?>
	<?php if(!$header_set && !$this->request->is('ajax')){ $header_set= true; ?>
	<tr class="t-head mob-hide">
		<td width="25%">Activity: </td>
		<td width="15%">Name:</td>
		<td width="13%">Date &amp; Time:</td>
		<td width="10%">Price:</td>
		<td width="15%"><?php echo ($_booking['Agent']['agent_type']==1)?'Commission:':'Discounted'; ?></td>
		<td width="12%">Status:</td>
	</tr>
	<?php  } ?>
	<tr>
		<td data-title="Activity:"><?php echo $_booking['Booking']['course_name']; ?></td>
		<td data-title="Name:"><?php echo $_booking['Guest']['first_name']; ?> <?php echo $_booking['Guest']['last_name']; ?> <?php echo $_booking['Booking']['pax']; ?>x</td>
		<td data-title="Date &amp; Time:"><?php echo date("d M Y",strtotime($_booking['Booking']['date'])); ?> <?php echo date("ha",strtotime($_booking['Booking']['start_time'])); ?> </td>
		<td data-title="Price:">US$<?php echo $_booking['Booking']['total']; ?></td>
		<td data-title="<?php echo ($_booking['Agent']['agent_type']==1)?'Commission:':'Discounted'; ?>">US$<?php echo $_booking['AgentTransaction']['amount']; ?></td>
		<td data-title="Status:"><?php echo $_booking['Booking']['is_completed']; ?></td>
	</tr>
	<?php $counter++; ?>
	<?php if(empty($agent_data['past_bookings'][$counter]['Booking']['date']) || ($current_month!=date("n",strtotime($agent_data['past_bookings'][$counter]['Booking']['date'])))){ ?>
	</table>
	<?php  } ?>
	<?php } ?>
<?php  } ?>