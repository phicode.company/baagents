<div class="message-notification">
	<div class="alert alert-error">
	<button class="close" data-dismiss="alert"></button>
	<?php echo h($message) ?>
	</div>
</div>

<script>
	jQuery(document).ready(function() {		
		jQuery(".message-notification").find(".close").bind('click',function(){
			jQuery(this).parent().parent().fadeOut(500);
		});
	});
</script>