<div class="upper-footer">
				<div class="col4 first">
	<h2>Commitment to Impact</h2>
	<p>From the inception of Backstreet Academy as a social enterprise in Kathmandu, Nepal, we’ve always maintained our belief that profit making and social impact go hand-in-hand. This mantra drives our team across all our offices and we wouldn’t have it any other way.
</p>
				<a href="/commitment-to-impact">More on our social impact</a>		</div>
	<div class="col4 ">
	<h2>Stories from the Backstreets</h2>
	<p>Everyday, Backstreet Academy sees&nbsp;new&nbsp;friendships forged, new experiences gained, new skills learnt and new memories created. Stories that are special and everlasting. The often inaccessible backstreets are a&nbsp;rare glimpse for a traveler,&nbsp;but we are changing that, one story at a time.
</p>
			<a href="/travelers-tales">Read the stories here</a>		</div>
	<div class="col4 last">
	<h2>Trust and Safety</h2>
	<p>Wherever you are around in Asia, Backstreet Academy is committed to the safest experiences for both travelers and hosts alike. That means setting the strictest safety standards possible, then working hard to improve them every day and&nbsp;ensure your comfort and security.
</p>
				<a href="/trust-and-safety">Learn about trust at Backstreet Academy</a>		</div>
		</div>


<div class="bottom-footer">
						
			<div class="col4 first"><h5>Discover</h5><ul><li><a href="/trust-and-safety" title="" target="_blank">Trust and Safety</a></li><li><a href="/why-host" title="">Why Host</a></li><li><a href="http://newsite.awake.travel/travelers-tales" title="Traveler Tales" target="_blank">Traveler Tales</a></li><li><a href="https://www.newsite.awake.travel/blog/" title="">Backstreet Guides</a></li><li><a href="https://www.newsite.awake.travel/blog/corporate-bookings-at-backstreet-academy/" title="Corporate Bookings with Backstreet Academy" target="_blank">For Corporates </a></li><li><a href="/faq" title="">FAQ</a></li></ul></div><div class="col4"><h5>Company</h5><ul><li><a href="/about-us" title="">About Us</a></li><li><a href="http://newsite.awake.travel/backstreet-in-press" title="">Press</a></li><li><a href="/responsible-hosting" title="">Responsible Hosting</a></li><li><a href="/faq" title="">FAQ</a></li><li><a href="/terms-of-service" title="">Terms of service</a></li><li><a href="/fees-and-booking-policy" title="">Fees and booking policy</a></li><li><a href="/privacy-policy" title="">Privacy policy</a></li></ul></div>					
			<div class="col4 last">
			 <div class="mail-123" style="margin-top:15px;">
			
				 <img src="/img/footer-email.png" alt="">			 </div>
			 <div class="place-number"></div>            
	 
             
            
             <div class="social-icon-footer">
                 <a href="/contact-us" class="footer-btn">Connect With Us</a>				 					<div class="icons-footer"><a href="www.facebook.com/backstreetacademy" target="_blank"><img src="/img/facebook-yellow.png" alt="Facebook"></a></div>
                 					<div class="icons-footer"><a href="https://twitter.com/The_Backstreets" target="_blank"><img src="/img/twitter-yellow.png" alt="Twitter"></a></div>
				 					<div class="icons-footer"><a href="http://www.pinterest.com/backstreetacad/" target="_blank"><img src="/img/pinterest-yellow.png" alt="Pinterest"></a></div>
				 					<div class="icons-footer"><a href="https://plus.google.com/+Backstreetacademy/posts" target="_blank"><img src="/img/google-plus-yellow.png" alt="Google+"></a></div>
				              </div>
             <div class="social-icon-footer">			
				              </div>
             
          </div>
		</div>