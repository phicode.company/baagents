<div class="logo-1"><a href="<?php echo Router::url('/', true);?>" >
<div class="back-logo"><span class="word-1">B</span><span class="word-2">A</span><span class="word-3">C</span><span class="word-4">K</span>STREET<br /><span class="acade">academy</span></div></a></div>
<div class="logo"><a href="<?php echo Router::url('/', true);?>">
<img src="<?=Configure::read('MainSite.logo');?>" /></a></div>
	<div class="left-menu">	
	</div>
	
	<?php //if(in_array($this->params['controller'],array('courses','users','profiles','account','bookings'))){?>
	
	
	<div class="search-results <?php //if($this->params['plugin']=="content_manager" && $this->params['action']=="home"){echo"home-search1";}?>">
		<?php $search = !empty($search)?$search:'';?>
		<?=$this->Form->create(false,array('url'=>array('controller'=>'courses','action'=>'search','plugin'=>'course_manager'),'id'=>'CourseSearch_Form'));?>
		<?=$this->Form->text('search',array('placeholder'=>'City or Activities','value'=>$search,'autocomplete'=>'off','class'=>'country-11','id'=>'CourseSearch'));?>	
		<div style="margin-top: 2%; right: 5px; top: 0px; display: none; position:absolute;" id="header-loader"><img alt="ajax-loader" src="/img/ajax-loader.gif"></div>		
		<?=$this->Form->submit('',array('div'=>false,'id'=>'search-record'));?>
		<?=$this->Form->end();?>
			<div class="toggle-country1">
				<?//=$this->element('CourseManager.country_city',array('include_script'=>true));?>
			</div>

	</div> 
	
	
	<?php //} ?>
	
	<div class="right-menu">
		<?php  $userId = $this->Session->read('Auth.Agent.id'); ?>
	<ul>
		<?php if(!empty($userId)){?>
			<li id="about_us"><?=$this->Html->link($this->Session->read('Auth.Agent.first_name'),array('controller' => 'agents', 'action' => 'dashboard'));?></li>
			<li id="about_us"><?=$this->Html->link('Dashboard',array('controller' => 'agents', 'action' => 'dashboard'));?></li>
			<li id="about_us"><?=$this->Html->link('Logout',array('controller' => 'agents', 'action' => 'logout'));?></li>
		<?php }else{ ?>
			<li id="view_signup"><a href="<?=Router::url(array('controller'=>'agents','action'=>'register'));?>" class="signup" data-link="signup">Sign up</a></li>
			<li id="view_login"><a href="<?=Router::url(array('controller'=>'agents','action'=>'login'));?>" class="login" data-link="login">Log In</a></li>
			<span class="box1"></span>
			<li style="display:none;"><?php echo $this->Html->link('Login',array('controller'=>'users','action'=>'login','plugin'=>'course_manager'),array('class'=>'various fancybox.ajax login','style'=>'display:none;'));?></li>
		<?php  }?>	
			
			
	</ul>
</div>
<?php $userId = $this->Session->read('Auth.Front.id');?>
<!--HTML is used for responsive only-->
<div class="course-list"><?= $this->Html->link('List Your Experience',array('controller'=>'pages','action'=>'get_started','plugin'=>'content_manager',39),array('class'=>'btn-yellow various fancybox.ajax'));?></div>
	<div class="responsive-toggle">
		<button class="nav-toggler toggle-push-right"><?php echo $this->Html->image('toggle-image.png',array('alt'=>''));?></button>
		<div class="menu nav push-menu-right">
			<ul>
				<?php if(empty($userId)){ ?>
				<li><?=$this->Html->link('About',array('controller' => 'pages', 'action' => 'view','plugin'=>'content_manager','3'));?></li>
				<?php } ?>
				<?php /*?><li><?=$this->Html->link('How It Works',array('controller' => 'pages', 'action' => 'view','plugin'=>'content_manager','2'));?></li><?php */?>
				<?php if(empty($userId)){ ?>
					<li><a href="javascript:void(0)" class="signup" data-link="signup">Sign up</a></li>
					<li><a href="javascript:void(0)" class="login" data-link="login">Login</a></li>
					<li style="display:none;"><?php echo $this->Html->link('Login',array('controller'=>'users','action'=>'login','plugin'=>'course_manager'),array('class'=>'login','style'=>'display:none;'));?></li>
				<?php } else { ?>
					<li><?php echo $this->Html->link('Dashboard',array('controller'=>'users','action'=>'dashboard','plugin'=>'course_manager'));?><?=(!empty($unreadMsgs) && $unreadMsgs > 0) ? '<span class="notification">'.$unreadMsgs.'</span>' : '';?></li>
					<li><a href="<?=Router::url(array('plugin'=>'course_manager','controller'=>'inbox','action'=>'index'));?>" class="<?php if(($this->params['controller']=="inbox") && (in_array($this->params['action'],array('index')))) echo "active"; ?>">Inbox</a></li>
					<li><a href="<?=Router::url(array('plugin'=>'course_manager','controller'=>'courses','action'=>'course_listing'));?>" class="<?php if(($this->params['controller']=="courses") && (in_array($this->params['action'],array('course_listing')))) echo "active"; ?>">Your Listings</a></li>
					<li><a href="<?=Router::url(array('plugin'=>'course_manager','controller'=>'bookings','action'=>'your_bookings'));?>" class="<?php if(($this->params['controller']=="bookings") && (in_array($this->params['action'],array('your_bookings')))) echo "active"; ?>">Your Bookings</a></li>
					<li><a href="<?=Router::url(array('plugin'=>'course_manager','controller'=>'courses','action'=>'user_wishlist'));?>" class="<?php if(($this->params['controller']=="courses") && (in_array($this->params['action'],array('user_wishlist')))) echo "active"; ?>">Wish List</a></li>
					<li><a href="<?=Router::url(array('plugin'=>'course_manager','controller'=>'profiles','action'=>'user_profile'));?>" class="<?php if(($this->params['controller']=="profiles") && (in_array($this->params['action'],array('user_profile')))) echo "active"; ?>">Profile</a></li>
					<li><a href="<?=Router::url(array('plugin'=>'course_manager','controller'=>'accounts','action'=>'notifications'));?>" class="<?php if(($this->params['controller']=="accounts") && (in_array($this->params['action'],array('notifications')))) echo "active"; ?>">Account</a></li>
					<li><?php echo $this->Html->link('Logout',array('controller'=>'users','action'=>'logout','plugin'=>'course_manager'),array('class'=>'logout'));?></li>
					<?php } ?>
					
				<li class="inviteYourFriends" style="<?//=$liStyle?>"><a href="<?php echo Router::url(array('controller'=>'invites','action'=>'index','plugin'=>'course_manager')); ?>" class="">Invite Friends</a></li>
				<li><?= $this->Html->link('List Your Experience',array('controller'=>'pages','action'=>'get_started','plugin'=>'content_manager',39),array('class'=>'login'));?></li>
				<li style="padding-left:10px;"><button class="close-menu">Close &rarr;</button></li>
			</ul>
		</div>
</div>
<!--end HTML is used for responsive only-->
