<?php /*
<?php if ($this->Session->check('Message.flash')): ?>
<div class="message-notification">
	<div class="alert alert-success">
	  <button class="close" data-dismiss="alert"></button>
	  <?=$this->Session->flash(); ?>
	</div>
</div>
<?php endif;?>
<?php if ($this->Session->check('Message.error')): ?>
<div class="message-notification">
	<div class="alert alert-error">
	<button class="close" data-dismiss="alert"></button>
	<?=$this->Session->flash(); ?>
	</div>
</div>
<?php endif;?>

<?php if ($this->Session->check('Message.auth')): ?>
<div class="message-notification">
	<div class="alert alert-error">
	<button class="close" data-dismiss="alert"></button>
	<?=$this->Session->flash(); ?>
	</div>
</div>
<?php endif;?>
<script>
	jQuery(document).ready(function() {		
		jQuery(".message-notification").find(".close").bind('click',function(){
			jQuery(this).parent().parent().fadeOut(500);
		});
	});
</script>
 */ ?>
<?php echo $this->Flash->render(); ?> 
 
