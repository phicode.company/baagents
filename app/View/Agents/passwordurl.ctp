<h3 class="heading-mt">Reset Password</h3>
<?=$this->element('message');?> 
<div class="white-background-full">
	<?php echo $this->Form->create('User', array('name' => 'user','url' => array('plugin'=>'course_manager','controller'=>'users','action'=>'passwordurl/'.$str),'novalidate'=>true,'class'=>'registration-form'));?>
		<div class="registration-form-row">
			<div class="col6 first">
			<div class="course-dates inputs inputs-4">
			<label class="label">New Password: <span style="color:#ff0000">*</span></label>
				<?=$this->Form->password('password',array('required'=>false)); ?>
				<?=$this->Form->hidden('form-name',array('required'=>false,'value'=>'PasswordUrlForm')); ?>
			</div>
		</div>
			<div class="col6 first">
			<div class="course-dates inputs inputs-4">
			<label class="label">Confirm Password: <span style="color:#ff0000">*</span></label>
				<?=$this->Form->password('confirm_password',array('required'=>false)); ?>
			</div>
		</div>
			<div class="save-btn">
			<div  style="text-align:right;display:block;width:60%;float:left;"><input type="submit" value="Save"></div>
			<div id="pre_loader">
			<?=$this->Html->image('preloader.png');?></div>
			</div>	
			</div>
		 <div class="clear"></div>
	<?php echo $this->Form->end();?>
</div>

<script type="text/javascript">
		<?php $path = $this->Html->webroot; ?>
		$(document).ready(function(){
		$('#UserPasswordurlForm').submit(function(){
			var data = new FormData(this);
			var formData = $(this);
			var status = 0;
			$.each(this,function(i,v){
					$(v).removeClass('invalid form-error');
					});
				$('.error-message').remove();
				$('#UserPasswordurlForm > span#for_owner_cms').show();
				$('#UserPasswordurlForm > button[type=submit]').attr({'disabled':true});
				$('#pre_loader').show();
			$.ajax({
					url: '<?=$path?>course_manager/users/ajax_validation',
					async: false,
					data: data,
					dataType:'json', 
					type:'post',
					cache: false,
					contentType: false,
					processData: false,
					success: function(data){
						$('#pre_loader').hide();
						if(data.error==1){
							$.each(data.errors,function(i,v){
					$('#'+i).addClass("invalid form-error").after('<div class="error-message">'+v+'</div>');
					$('#'+i).bind('click',function(){
					$(this).removeClass('invalid form-error');
					$(this).next('.error-message').remove();
					});
							});
						}else{
							status = 1;
						}
					}
				});
				if(status==0){
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$('#UserPasswordurlForm > button[type=submit]').attr({'disabled':false});
				$('#UserPasswordurlForm > span#for_owner_cms').hide();
				}
			return (status===1)?true:false; 
			});
		});
</script>

 
