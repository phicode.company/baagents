<!--Popup for login-->
<h3 class="heading-mt"></h3>
<div class="white-back-full">
	<div class="fields-page agent-register">
		<div class="">
			<div id="login">
			<div class="right-popup-content">
				<h4>Agent Portal</h4>
				<div style="/*display:none;*/"><?php echo $this->Element('message');?></div>
				<div class="facebook-btn"><!--<a href="javascript:void(0)" onclick="authorizeAppInPopup()"><img src="/img/facebook-login-btn.png" alt="Login with Facebook"></a><div class="or">OR</div>--></div>
				
				<?php echo $this->Form->create('Agent',array('name'=>'user','id'=>'AgentLogin' ,'novalidate' => true, 'class'=>''));?>
					<?=$this->Form->hidden('form-name',array('required'=>false,'value'=>'AgentLoginForm')); ?>
					<?//='asdf'.$user_email.' - '.$user_password;die;?>
					<?=$this->Form->email('email',array('required'=>false,'placeholder'=>'Email Address','id'=>'AgentEmail_1')); ?><?=$this->Form->error('email',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					<?=$this->Form->password('password',array('required'=>false,'placeholder'=>'Password','id'=>'AgentPassword_1')); ?><?=$this->Form->error('password',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					<div class="extra">
						<div class="left">
							<?=$this->Form->checkbox('remember', array('hiddenField' => false));?><label for="UserRemember">Remember Me</label>
						</div>
						<div class="right">
							<?=$this->Html->link('Forgot Password?',array('controller' => 'agents', 'action' => 'resetpassword','plugin'=>false),array('class'=>'various fancybox.ajax'));?>
							<span style="color:#ffb900"> | </span>
							<a href="<?=Router::url(array('controller'=>'agents','action'=>'register'))?>" class="various">Sign up</a>
							</li>
						</div>
					</div>
					<div>
						<input type="submit" value="Login"/>
						<div id="pre_loader" class="reloader"><?=$this->Html->image('preloader.png');?>
						</div>
					</div>
				<?php echo $this->Form->end();?>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<!--end #login-->
	
<script type="text/javascript">
  $(document).ready(function(){
	  $('#AgentLogin').submit(function(){
		  var Fieldsdata = new FormData(this);
		  var formData = $(this);
		  var status = 0;
		  $.each(this,function(i,v){
			  $(v).removeClass('invalid form-error');
		  });
		  $('.invalid-login').remove();
		  $('.error-message').remove();
		  $('#AgentLogin > span#for_owner_cms').show();
		  $('#AgentLogin > button[type=submit]').attr({'disabled':true});
		  $('.reloader').show();
		  $.ajax({
				  url: '<?=Router::url(array('controller'=>'agents','action'=>'ajax_validation'))?>',
				  async: false,
				  data: Fieldsdata,
				  dataType:'json', 
				  type:'post',
				  cache: false,
				  contentType: false,
				  processData: false,
				  success: function(data) {						
					  if(data.error==1){
						  $('.reloader').hide();
						  $.each(data.errors,function(i,v){
						  $('#'+i+'_1').addClass("invalid form-error").after('<div class="error-message">'+v+'</div>');
							  $('#'+i+'_1').bind('click',function(){
								  $(this).removeClass('invalid form-error');
								  $(this).next('.error-message').remove();
							  });
						  });
					  }else{
						  status = 1;
					  }
				  
				  }
			  });
			  if(status==0){
				  //$("html, body").animate({ scrollTop: 0 }, "slow");
				  $('#AgentLogin > button[type=submit]').attr({'disabled':false});
				  $('#AgentLogin > span#for_owner_cms').hide();
				  return false; 
			  }
		  return (status===1)?true:false; 
		  });
	  });
</script> 