<div class="agent">
	<h2>Agent Booking Platform</h2>
<?php echo $this->Element('message');?>
	<div class="white-background agent-booking-info">
		<?php if($agent_data['profile']['agent_type']==1){ ?>
		<table width="100%" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td>Account:</td>
					<td>Total Bookings:</td>
					<td>Total Commissions:</td>
					<td>Commissions Cashed out:</td>
					<td>Balance:</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-title="Account:"><?=$agent_data['profile']['company_name']?></td>
					<td data-title="Total Bookings:"><?=$agent_data['summary']['total_booking']?></td>
					<td data-title="Total Commissions:">US$<?=$agent_data['summary']['total_commission']?></td>
					<td data-title="Commissions Cashed out:">US$<?=$agent_data['summary']['total_commission_cashout']?></td>
					<td data-title="Balance:">US$<?=$agent_data['summary']['total_balance']?></td>
				</tr>
			</tbody>
		</table>
		<?php } else{ ?>
		<table width="100%" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td>Account:</td>
					<td>Total Bookings:</td>
					<td>Last Payment:</td>
					<td>Balance To Pay:</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-title="Account:"><?=$agent_data['profile']['company_name']?></td>
					<td data-title="Total Bookings:"><?=$agent_data['summary']['total_booking']?></td>
					<td data-title="Last Payment:">US$<?=$agent_data['summary']['last_payment_received']?></td> 
					<td data-title="Balance To Pay:">US$<?=$agent_data['summary']['total_payment_balance']?></td>
					
				</tr>
			</tbody>
		</table>
		
		<?php } ?>
	</div><!--white background close-->
	
	
	
	<?php echo $this->Form->create('Booking',array('name'=>'user','id'=>'AgentBooking' ,'novalidate' => true, 'class'=>''));?>
	<?php echo $this->Form->hidden('booking_id'); ?>
	<?php echo $this->Form->hidden('price',array('id'=>'course_price')); ?>
	<?php echo $this->Form->hidden('booking_type'); ?>
	<?php echo $this->Form->hidden('end_time',array('autocomplete'=>'off','id'=>'course_end_time'));?>
	<?php echo $this->Form->hidden('discount',array('value'=>'0','id'=>'course_discount'));?>
	<?php echo $this->Form->hidden('ba_host_amount',array('value'=>0,'id'=>'course_ba_host_amount'));?>
	<?php echo $this->Form->hidden('ba_amount',array('value'=>0,'id'=>'course_ba_amount'));?>
	<?php echo $this->Form->hidden('facilitator_amount',array('value'=>0,'id'=>'course_facilitator_amount'));?>
	<?php echo $this->Form->hidden('transportator_amount',array('value'=>0,'id'=>'course_transportator_amount'));?>
	<?php echo $this->Form->hidden('surcharge',array('value'=>0,'id'=>'course_surcharge'));?>
	<?php echo $this->Form->hidden('ba_host_surcharge_amount',array('value'=>0,'id'=>'course_ba_host_surcharge_amount'));?>
	<?php echo $this->Form->hidden('service_fee',array('id'=>'course_service_fee')); ?>
	<?php echo $this->Form->hidden('host_fee',array('id'=>'course_host_fee')); ?>
	<?php echo $this->Form->hidden('coupon_code'); ?>
	<?php echo $this->Form->hidden('coupon_discount',array('value'=>'0','id'=>'course_coupon_discount'));?>
	<?php echo $this->Form->hidden('is_referral',array('value'=>'0','id'=>'course_is_referral'));?>
	<?php echo $this->Form->hidden('coupon_user',array('value'=>'0','id'=>'course_coupon_user'));?>
	<?php echo $this->Form->hidden('total',array('value'=>'0','id'=>'course_total'));?>
	<?php echo $this->Form->hidden('due_now',array('value'=>'0','id'=>'course_due_now'));?>
	<?=$this->Form->hidden('addons',array('value'=>''))?>
	
	<div class="white-background agent-booking-form">
		<h3>Make new Booking</h3>
		<div class="left-sec">
			<div class="row">
				<div class="field one-half">
					<?=$this->Form->input('country_id',array('required'=>false,'placeholder'=>'Country','options'=>$countries,'empty'=>'Select Country','label'=>false,'class'=>'chosen-select addon2_selectdropdown')); ?>
					<?=$this->Form->error('country_id',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
				</div>
				<div class="field one-half">
					<?=$this->Form->input('city_id',array('required'=>false,'placeholder'=>'City','label'=>false,'empty'=>'Select City','options'=>$cities,'class'=>'chosen-select addon2_selectdropdown')); ?>
					<?=$this->Form->error('city_id',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
			</div><!--row close-->
			<div class="row">
				<div class="field one-half">
					<?=$this->Form->text('first_name',array('required'=>false,'placeholder'=>'First Name','label'=>false)); ?>
					<?=$this->Form->error('first_name',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
				<div class="field one-half">
					<?=$this->Form->text('last_name',array('required'=>false,'placeholder'=>'Last Name','label'=>false)); ?>
					<?=$this->Form->error('last_name',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
			</div><!--row close-->
			<div class="row">
				<div class="field one-third">
					<?=$this->Form->text('email',array('required'=>false,'placeholder'=>'Email','label'=>false)); ?>
					<?=$this->Form->error('email',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
				</div>
				<div class="field one-third">
					<?=$this->Form->text('mobile',array('required'=>false,'placeholder'=>'Mobile No.','label'=>false)); ?>
					<?=$this->Form->error('mobile',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
				<div class="field one-third">
					<?=$this->Form->text('roomno',array('required'=>false,'placeholder'=>'Room No','label'=>false)); ?>
					<?=$this->Form->error('roomno',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
			</div><!--row close-->
			<div class="row">
				<div class="field full">
					<?=$this->Form->input('course_id',array('required'=>false,'placeholder'=>'Course','label'=>false,'empty'=>'Select Course','options'=>$courses,'class'=>'chosen-select addon2_selectdropdown')); ?>
					<?=$this->Form->error('course_id',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
				
			</div><!--row close-->
			<div class="row addon-section">
				
			</div>
			<div class="row">
				<div class="field one-third">
					<?=$this->Form->input('date',array('required'=>false,'placeholder'=>'Date','label'=>false,'empty'=>'Date','options'=>$dates,'class'=>'chosen-select addon2_selectdropdown')); ?>
					<?=$this->Form->error('date',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
				<div class="field one-third">
					<?=$this->Form->input('time',array('required'=>false,'placeholder'=>'Time','label'=>false,'empty'=>'Time','options'=>$dates,'class'=>'chosen-select addon2_selectdropdown')); ?>
					<?=$this->Form->error('time',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
				<div class="field one-third">
					<?=$this->Form->input('pax',array('required'=>false,'placeholder'=>'Pax','label'=>false,'empty'=>'Pax','options'=>$dates,'class'=>'chosen-select addon2_selectdropdown')); ?>
					<?=$this->Form->error('pax',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
				</div>
			</div><!--row close-->
		</div><!--left close-->
		<div class="right-sec">
			<div class="row">
				<div class="field pickup_input">
					<label class="location">Pickup Location:</label>
				
					<?php echo $this->Form->text('pickup_address',array('placeholder'=>'Enter Your Pickup Location','id'=>'pickup_address'));?>
					<?php echo $this->Form->error('pickup_address',null,array('wrap'=>'div','class'=>'error-message'));?>
				</div>
				<div class="field pickup_text" style="display:none;">
					<div class="text-add">
						<span>Address:</span> Taj Lake Palace, Udaipur, Rajasthan, India, Udaipur, India<br>
						(This is test address)
					</div>
					
				</div>
				
				<?php echo $this->Form->hidden('lat',array('id'=>'pickup_lat'));?>
				<?php echo $this->Form->hidden('long',array('id'=>'pickup_long'));?>
			 
				 <div class="full pickup_input">
					<div class="text-l"><p>Is the corresponding pin reflected on the map correct?</p></div>
					 <div class="yes-no" id="CourseIsMapCorrect">
						<?=$this->Form->radio('is_map_correct',array('1'=>''),array('class'=>'css-checkbox','hiddenField'=>false,'legend'=>false,'label'=>false,'default'=>''));?>
						<?php echo $this->Form->label('is_map_correct1','Yes',array('class'=>'css-label'));?>
						<?=$this->Form->radio('is_map_correct',array('0'=>''),array('class'=>'css-checkbox css-checkbox-1','hiddenField'=>false,'legend'=>false,'label'=>false));?>
						<?php echo $this->Form->label('is_map_correct0','No',array('class'=>'css-label css-label-1'));?>
					</div>
					<?php echo $this->Form->error('is_map_correct',null,array('wrap'=>'div','class'=>'error-message'));?>
				</div>
			</div>
			<div class="map-box">
				<div class="map-location" id="map-location" style="height:300px; margin-top:0px;">
				<!-- Will show map -->
			</div>
			</div>
		</div><!--right-sec close-->
		<div class="action-box">
			<div class="agent-booking-info">
				<ul>
					<li>Total Price:<span id="total_price_text">US$</span><span id="discounted_total">00.00</span> <span id="amount_in_local_currency"></span></li>
					<?php if($agent_data['profile']['agent_type']==1){ ?>
						<li>Commission:<span id="total_commission">US$</span><span id="agent_commission">00.00</span>  <span id="agent_commission_in_local_currency"></span></li>
					<?php  } ?>
					
					<?php if($agent_data['profile']['agent_type']==2){ ?>
						<li>Discounted:<span id="total_discounted">US$</span><span id="agent_discounted">00.00</span>  <span id="agent_discounted_in_local_currency"></span></li>
					<?php  } ?>
					<li>Transport:<span id="transport-html"></span></li>
				</ul>
			</div><!--agent-booking-info close-->
			<div class="btn">
				<input type="submit" value="Submit Booking" />
			<div id="pre_loader" class="reloader" style="display: none;"><img src="/img/preloader.png" alt="">						</div>
			</div>
		</div><!--action-box close-->
	</div><!--agent-booking-form close-->
	
	<?php echo $this->Form->end();?>
	
	<?php if(!empty($agent_data['bookings'])){ ?>
	<div class="white-background agent-past-booking">
		<h3>Upcoming Bookings:</h3>
		<div class="future-booking-loop">
		<?php echo $this->element('future_booking_loop'); ?>
		</div>
		<?php if(!empty($future_booking_params['nextPage'])){?>
		<div class="see-more future-booking-see-more-link">
			<a href="#">See more</a>
		</div>
		<?php  } ?>
	</div><!--agent-past-booking close-->
	<?php } ?>
	
	<?php if(!empty($agent_data['past_bookings'])){ ?>
	<div class="white-background agent-past-booking">
		<h3>Past Bookings:</h3>
		<div class="past-booking-loop">
		<?php echo $this->element('past_booking_loop'); ?>
		</div>
		<?php if(!empty($past_booking_params['nextPage'])){?>
		<div class="see-more past-booking-see-more-link">
			<a href="#">See more</a>
		</div>
		<?php  } ?>
	</div><!--agent-past-booking close-->
	<?php } ?>
	
</div><!--agent close-->

<div id="divBookingupdate" style="display:none;">
	<div class="fancybox-outer">
		<div class="fancybox-inner" >
			<div class="full-content pop-up">
				<h3>Modify Booking</h3>
				<div class="white-top-bg">
					<div class="wrapper">
						<div style="color:green" id="success-msg"></div>
						<div id="google_code"></div>
						<span id="showerrors" style="color:red"></span>
							<?php echo $this->Form->create('Booking',array('name'=>'user','id'=>'AgentModifyBooking' ,'novalidate' => true, 'class'=>''));?>
							<?php echo $this->Form->hidden('booking_id'); ?>
							<?php echo $this->Form->hidden('name',array('value'=>'BookingModify')); ?>
						
							<div class="booking-message"></div>
							<table class="booking-request-popup" width="100%" cellspacing="0" cellpadding="0">
							<tbody>
							<tr class="theading">
							<td class="text-simple" style="width:40%;">Date</td>
							<td class="text-simple" style="width:40%;">Course Time</td>
							<td class="text-simple" style="width:20%;">Pax</td>
							</tr>
							<tr>
							<td class="datepicker-1 date">
								<?=$this->Form->input('date',array('required'=>false,'placeholder'=>'Date','label'=>false,'empty'=>'Date','options'=>$dates,'class'=>'')); ?>
								<?=$this->Form->error('date',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
							</td>
							<td class="time c-time">
								<?=$this->Form->input('time',array('required'=>false,'placeholder'=>'Time','label'=>false,'empty'=>'Time','options'=>$dates,'class'=>'')); ?>
								<?=$this->Form->error('time',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
							</td>
							<td class="pax-1 b-pax"><?=$this->Form->text('pax',array('required'=>false,'placeholder'=>'Pax','label'=>false,'empty'=>'Pax','class'=>'','disabled'=>'disabled')); ?></td>
							</tr>
							</tbody>
							</table>
	
	
	
	
							<div class="save-btn br-form-btn"><div class="submit"><input id="request_contact_form" value="Modify Booking" type="submit"></div><div id="Booking_request_loader" style="margin-top: 2%; position: absolute; right: 80px; top: 5px; display: none;"><img src="/img/ajax-loader.gif" alt=""></div>
						</div>
						<?php echo $this->Form->end();?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<script type="text/javascript">
var default_latitude = '<?php echo ($agent_data['profile']['location']['city_latitude'])?$agent_data['profile']['location']['city_latitude']:'-34.397' ?>';
var default_longitude = '<?php echo ($agent_data['profile']['location']['city_longitude'])?$agent_data['profile']['location']['city_longitude']:'150.644' ?>';
var addon_response = [];

$(document).ready(function(){
	var pickup_location_status = 0;
	<?php /* if(!empty($agent_data['profile']['location']['city_name']) && !empty($agent_data['profile']['location']['country_name']) ){ ?>
		$.ajax({
			url: 'https://maps.googleapis.com/maps/api/geocode/json?address=<?php echo str_replace(" ","+",$agent_data['profile']['location']['city_name']) ?>,<?php echo str_replace(" ","+",$agent_data['profile']['location']['country_name']) ?>&key=AIzaSyD-Z5RORk7Oeo2L-kralZOoBaEo9yec2YI',
			async: false,
			dataType:'json', 
			type:'get',
			success: function(data) {
				console.log(data);
				//alert(data.results.geometry.location.lat);
				default_latitude = data.results[0].geometry.location.lat;
				default_longitude = data.results[0].geometry.location.lng;
			}
		});
	<?php  } */ ?>
	
	
		$(".various2").fancybox({
				type:'inline',
				href: '#divBookingupdate',
				afterShow: function(){
					$('.fancybox-skin').addClass('booking-request-popup');
				},
			beforeLoad: function(){
					var booking_id = $(this.element[0]).attr('attr-id');
					var booking_data;
					if(booking_id){
						$.ajax({
							url: '<?=Router::url(array('controller'=>'agents','action'=>'loadbooking'))?>',
							async: true,
							data: {booking_id:booking_id},
							dataType:'json', 
							type:'post',
							success: function(data) {
								if(data.status=="success"){
									booking_data = data.results.Booking;
									$('#AgentModifyBooking input[name="data[Booking][pax]"]').val(data.results.Booking.pax);
									loaddatesdropdown(data.results.Booking.course_id,data.results.Booking.date,data.results.Booking.start_time);
									
								}
							}
						});
					}
					
					function loaddatesdropdown(course_id,date,time){
						$('#AgentModifyBooking select[name="data[Booking][date]"]').html('<option value="" >Date:</option>');
						$.ajax({
							url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCourseDates'))?>',
							async: true,
							data: {course_id:course_id},
							dataType:'json', 
							type:'post',
							
							success: function(data) {
								if(data.status=="success"){
									$.each(data.results,function(i,v){
										if(i==date){
											$('#AgentModifyBooking select[name="data[Booking][date]"]').append('<option value="'+i+'" selected>'+v+'</option>');
										}else{
											$('#AgentModifyBooking select[name="data[Booking][date]"]').append('<option value="'+i+'">'+v+'</option>');
										}
									});
									loadtimedropdown(course_id,time);
								}
							}
						});
					
					}
					
					
					function loadtimedropdown(course_id,selected_time){
						$('#AgentModifyBooking select[name="data[Booking][time]"]').html('<option value="" >Time:</option>');
						var date = $('#AgentModifyBooking select[name="data[Booking][date]"]').val();
						$.ajax({
								url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCourseTimming'))?>',
								async: true,
								data: {course_id:course_id,date:date},
								dataType:'json', 
								type:'post',
								success: function(data) {
									if(data.status=="success"){
										$.each(data.results,function(i,v){
											if(i==selected_time){
												$('#AgentModifyBooking select[name="data[Booking][time]"]').append('<option value="'+i+'" selected>'+v+'</option>');
											}else{
												$('#AgentModifyBooking select[name="data[Booking][time]"]').append('<option value="'+i+'">'+v+'</option>');
											}
										});
									}
								}
							});
					}
					
					$('#AgentModifyBooking select[name="data[Booking][date]"]').unbind('change').bind('change',function(){
						loadtimedropdown(booking_data.course_id,$(this).val());
					});
					
					$('#AgentModifyBooking select[name="data[Booking][time]"]').unbind('change').bind('change',function(){
						check_total();
					});
					
					function check_total(){
						var course_id = booking_data.course_id;
						var date = $('#AgentModifyBooking select[name="data[Booking][date]"]').val();
						var time = $('#AgentModifyBooking select[name="data[Booking][time]"]').val();
						var pax = booking_data.pax;
						var status = false;
						$.ajax({
							url: '<?=Router::url(array('controller'=>'agents','action'=>'calculate'))?>',
							async: false,
							data: {
									course_id:course_id,
									date:date,
									pax: pax,
									time:time},
							dataType:'json', 
							type:'post',
							beforeSend: function() {
								// make nice image to show the request is running
								$("#AgentModifyBooking #Booking_request_loader").show();
								$('#AgentModifyBooking .booking-message').html('');
								$("#AgentModifyBooking input[type='submit']").attr('disabled',true);
							},
							success: function(Data) {
								$("#AgentModifyBooking #Booking_request_loader").hide();
								$("#AgentModifyBooking input[type='submit']").attr('disabled',false);
								if(Data.status=="error"){
									status = false;
									$('#AgentModifyBooking .booking-message').html(Data.errorDescription);
								}
								
								if(Data.status=="success"){
									status = true;
								}
							}
						});
						return status;
					}
					
					$('#AgentModifyBooking').unbind('submit').submit(function(){
					
						
						if(check_total()){
							$.ajax({
								url: '<?=Router::url(array('controller'=>'agents','action'=>'updateBooking'))?>',
								async: false,
								data: {
										id: booking_data.id,
										date:$('#AgentModifyBooking select[name="data[Booking][date]"]').val(),
										time:$('#AgentModifyBooking select[name="data[Booking][time]"]').val()
										},
								dataType:'json', 
								type:'post',
								beforeSend: function() {
									// make nice image to show the request is running
									$('#AgentModifyBooking .booking-message').removeClass('error').html('');
									$("#AgentModifyBooking input[type='submit']").attr('disabled',true);
								},
								success: function(data){
									//console.log(data);
									//return false;
									$("#AgentModifyBooking input[type='submit']").attr('disabled',false);
									if(data.status=="success"){
										$('#AgentModifyBooking .booking-message').html('Booking Date/Time is updated successfully');
										$('#bookingdate-'+booking_data.id).html(data.results.date + ' '+data.results.time );
										
									}
									
									if(data.status=="error"){
										$('#AgentModifyBooking .booking-message').addClass('error').html(data.errorDescription);	
									}
								}
							});
						}else{
							
						}
						//$.fancybox.close(true);
						return false;
					});
				
				}
			});
	
	
	var futurebooking_page = <?php echo (int)$future_booking_params['page']; ?>;
	var pastbooking_page = <?php echo (int)$past_booking_params['page']; ?>;
	function loadcity(countryid){
		$('#AgentBooking #BookingCityId').html('<option value="">Select City:</option>');
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCity'))?>',
			async: true,
			data: {country_id:countryid},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					$.each(data.results,function(i,v){
						$('#AgentBooking #BookingCityId').append('<option value="'+i+'">'+v+'</option>');
						});
					$("#BookingCityId").trigger("chosen:updated");
				}
			}
		});
		
	}
	
	function loadCourse(cityid){
		$('#AgentBooking #BookingCourseId').html('<option value="">Select Course:</option>');
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCourse'))?>',
			async: true,
			data: {city_id:cityid},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					//$('#AgentBooking #BookingCourseId').append('<option value="">Select Course:</option>');
					$.each(data.results,function(i,v){
						$('#AgentBooking #BookingCourseId').append('<option value="'+i+'">'+v+'</option>');
						});
					$("#BookingCourseId").trigger("chosen:updated");
				}
			}
		});
		
	}
	
	function loadAddon(courseid){
		var addon_selected_value = $('#AgentBooking #BookingAddons').val();
		$('#AgentBooking #BookingAddons').html('<option value=\'[]\'>Add-ons:</option>');
		var cityid = $('#AgentBooking #BookingCityId').val();
		var pax = $('#AgentBooking #BookingPax').val()
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadAddons'))?>',
			async: true,
			data: {course_id:courseid,city_id:cityid,pax:pax},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				$('.addon-section').html('');
				if(data.status=="success"){
					//$('#AgentBooking #BookingCourseId').append('<option value="">Select Course:</option>');
					addon_response = data.results; 
					var html = '';
					$.each(data.results,function(i,v){
						
						html = '<div class="row"><div class="field full"><select name="data[Booking][addons2][]" class="chosen-select addon2_selectdropdown"><option value="">Addon:</option>';
						$.each(v,function(j,k){
							html += '<option value="'+k.add_on_id+'-'+k.add_on_pax+'">'+k.add_on_name+' - Pax: '+k.add_on_pax+' - Amount: '+ k.amount+' </option>';
							//$('.addon-section').append('<option value="'+k.add_on_id+'">'+k.add_on_name+'</option>');
						});
						html += '</select></div></div>';
						
						$('.addon-section').append(html);
						
						if(addon_selected_value==i){
							//$('#AgentBooking #BookingAddons').append('<option value=\''+i+'\' selected="selected">'+v+'</option>');
						}else{
						//	$('#AgentBooking #BookingAddons').append('<option value=\''+i+'\'>'+v+'</option>');
						}
					});
					
				}
				
				$('.addon-section select').unbind('change').bind('change',function(){
					createAddOnObject();
					});
				$('.addon-section select.chosen-select').chosen({inherit_select_classes: true});
				//$(".addon-section select").trigger("chosen:updated");
				adjust_action_box();
			}
		});
		//$(".addon-section select").trigger("chosen:updated");
		
	}
	
	
	function createAddOnObject(){
		var _addon = [];
			$('.addon-section select').each(function(i,v){
				if($(v).val()!=''){
					var ids = $(v).val().split("-");
					$.each(addon_response,function(i,v){
						if(i==ids[0]){
							$.each(v,function(k,j){
								if(ids[1]==j.add_on_pax){
									_addon.push(j);	
								}
							});
						}
					});
				}
				
				
				});
			
			$('#AgentBooking #BookingAddons').val(JSON.stringify(_addon));
			calculate_total();
	}
	function loadCourseDate(courseid){
		$('#AgentBooking #BookingDate').html('<option value="">Date:</option>');
		//var cityid = $('#AgentBooking #BookingDate').val();
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCourseDates'))?>',
			async: true,
			data: {course_id:courseid},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					//$('#AgentBooking #BookingCourseId').append('<option value="">Select Course:</option>');
					$.each(data.results,function(i,v){
						$('#AgentBooking #BookingDate').append('<option value="'+i+'">'+v+'</option>');
						});
				$("#BookingDate").trigger("chosen:updated");
				}
			}
		});
		
	}
	
	function loadCoursePickuplocation(courseid){
		//$('#AgentBooking #BookingDate').html('<option value="">Date:</option>');
		//var cityid = $('#AgentBooking #BookingDate').val();
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCoursePickupDetail'))?>',
			async: true,
			data: {course_id:courseid},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					pickup_location_status = data.results.location_status; 
					if(data.results.location_status==0){
						$('#AgentBooking .pickup_input').show();
						$('#AgentBooking .pickup_text').html('').hide();
						$('#pickup_lat').val('');
						$('#pickup_long').val('');
						$('#transport-html').html('Provided');
						load_host_pickup_location(default_latitude, default_longitude);
						
					}else{
						$('#AgentBooking .pickup_text').html('<div class="text-add"><span>Address:</span>'+data.results.pickup_address_html+'</div>').show();
						$('#AgentBooking .pickup_input').hide();
						$('#pickup_lat').val(data.results.lat);
						$('#pickup_long').val(data.results.long);
						$('#transport-html').html('Not Provided');
						load_host_pickup_location(data.results.lat,data.results.long);
					}
				}
				adjust_action_box();
			}
		});
	}
	
	
	
	function loadCourseTimming(date){
		$('#AgentBooking #BookingTime').html('<option value="">Time:</option>');
		var courseid = $('#AgentBooking #BookingCourseId').val();
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCourseTimming'))?>',
			async: true,
			data: {course_id:courseid,date:date},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					//$('#AgentBooking #BookingCourseId').append('<option value="">Select Course:</option>');
					$.each(data.results,function(i,v){
						$('#AgentBooking #BookingTime').append('<option value="'+i+'">'+v+'</option>');
						});
				$("#BookingTime").trigger("chosen:updated");
				}
			}
		});
		
	}
	
	
	function loadCoursePax(time){
		$('#AgentBooking #BookingPax').html('<option value="">Pax:</option>');
		var courseid = $('#AgentBooking #BookingCourseId').val();
		var date = $('#AgentBooking #BookingDate').val();
		
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCoursePax'))?>',
			async: false,
			data: {course_id:courseid,date:date,time:time},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					// append surcharge pax
					if(data.results.surcharge){
						$.each(data.results.surcharge,function(j,v){
							$('#AgentBooking #BookingPax').append('<option value="'+v.surcharge_pax+'" style="font-weight:bold;color:#ffb900;">'+v.surcharge_pax+'</option>');
						});
					}
					
					
					$.each(data.results.pax,function(i,v){
						$('#AgentBooking #BookingPax').append('<option value="'+v+'">'+v+'</option>');
					});
				$("#BookingPax").trigger("chosen:updated");
				}
			}
		});
	}
	
	function calculate_total(){
		var is_valid = 1;
		if($('#AgentBooking #BookingDate').val()==''){
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingTime').val()==''){
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingPax').val()==''){
			is_valid = 0;
		}
		
		if(is_valid==0){
			form_data = {pax:'1',total_pax_amount:'0',service_fee:'0',host_fee:'0',sub_total:'0',discount:'0',total:'0'};
			$.each(form_data, function(ii,vv){
				$('#'+ii).html(vv);
				$('#course_'+ii).val(vv);
			});
			//$('#CourseDetailsForm').find('input[type=submit]').attr('disabled',true);
			return false;
		}
		
		$('.error-message').remove();
		$('.AddOns').remove();
		var form_Data = document.getElementById("AgentBooking");
		var data = new FormData();
		var status = 0;
		var course_id = $('#AgentBooking #BookingCourseId').val();
		var add_ons = $('#AgentBooking #BookingAddons').val();
		var date = $('#AgentBooking #BookingDate').val();
		var time = $('#AgentBooking #BookingTime').val();
		var pax = $('#AgentBooking #BookingPax').val();
		
		data.append('course_id',course_id);
		data.append('date',date);
		data.append('add_ons',add_ons);
		data.append('pax',pax);
		data.append('time',time);
		
		
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'calculate'))?>',
			async: false,
			data: data,
			dataType:'json', 
			type:'post',
			cache: false,
			contentType: false,
			processData: false,
			//timeout:1000,
			beforeSend: function() {
				// make nice image to show the request is running
				$("#pre_loader").show();
				$("#AgentBooking input[type='submit']").attr('disabled',true);
			},
			success: function(Data) {
				//return;
				setTimeout(function(){
					$("#pre_loader").hide();
					$("#AgentBooking input[type='submit']").attr('disabled',false);
				}, 500);
				
				$.each(Data.results, function(i,v){
					$('#'+i).html(v);
					if(i=='discount'){
						if(v!=0){
							$('#discount_info').html(' - Discount ($'+v+')');
						} else {
							$('#discount_info').html(' ');
						}
					}
					$('#course_'+i).val(v);
					
					//update total booking fee of addons popup
					if (i=='total') {
                        $('.course_total').html(v);
                    }
					//update total addon fee on addons popup
					if (i=='AddOn') {
                        var addon_total = 0.00;
						$(v).each(function(k,l){
							addon_total += parseFloat(l.total_amount);
							});
						$('.addon_total').html(addon_total);
						if (addon_total > 0) {
                            $('#by_pass_addon_popup').val(1);
                        } else{
							//$('#by_pass_addon_popup').val(0);
						}
                    }
					//END
					
					if(v==0){
						$('#course_'+i+'_tr').hide();
						$('#course_'+i+'_field').hide();
						if(i=='coupon_discount'){
							$('#couponCode_tr').show();
						}
						if(i=='maximum_deductible_amount'){
							$('#couponCodeToolTip').hide();
							$('ins.dark-tooltip').find('#maximum_deductible_amount').html(v);
						}
					}else{
						$('#course_'+i+'_tr').show();
						$('#course_'+i+'_field').show();
						if(i=='coupon_discount'){
							$('#couponCode_tr').hide();							
						}
						if(i=='maximum_deductible_amount'){
							$('#couponCodeToolTip').show();
							$('ins.dark-tooltip').find('#maximum_deductible_amount').html(v);
						}
					}
					
					/*if(i=='is_referral'){
						if(v > 0){
							$('#discountType').html('Referral Discount');							
						}else{
							$('#discountType').html('Coupon Discount');
						}					
					}else{
						$('#discountType').html('Coupon Discount');
					}*/
				});
				/*
				if(Data.results.AddOn.length > 0){
					var Tr_id = 'CoursePaxDiv';
					$.each(Data.AddOn, function(index,addon){						
						var this_tr_id = 'TRAddOn'+addon.add_on_id;
						$('#'+Tr_id).after('<tr class="AddOns" id="'+this_tr_id+'">'+
							'<td align="left">'+addon.add_on_name+'</td>'+
							'<td align="right">US$'+addon.total_amount+'<span id="booking_fee"></span></td>'+
						'</tr>');
						Tr_id = this_tr_id;
					});
				}
				*/
				user_status = Data.user_status;
				is_error = Data.error;
				if(Data.results.error == 0){
					status = 1;
					//$(form_Data).find('input[type=submit]').attr('disabled',true);
					//$('error-msg').html()
				}else{
					$('a.user-login').unbind('click').bind('click', function(){//When referral code entered
						$.fancybox({
							closeEffect : 'none',
							href: '<?=Router::url(array('controller'=>'users','action'=>'do_login','applyCouponCode'));?>',
							type: 'ajax'
						});
					});
					//$(form_Data).find('input[type=submit]').attr('disabled',false);
				}
				
			},
			error: function(jqXHR, exception, msg) {
				setTimeout(function(){
					$("#pre_loader").hide();
				}, 500);
				 if (jqXHR.status === 0) {
					alert('Not connect, Verify Network.');
				} else if (jqXHR.status == 404) {
					alert('Requested page not found. [404]');
				} else if (jqXHR.status == 500) {
					alert('Internal Server Error [500].');
				} else if (exception === 'parsererror') {
					alert('Requested JSON parse failed.');
				} else if (exception === 'timeout') {
					alert('Time out error.');
				} else if (exception === 'abort') {
					alert('Ajax request aborted.');
				} else {
					alert('Uncaught Error.\n' + jqXHR.responseText);
				}
			}
		});
		return status==1 ? true : false;
		
		
		
	}
	
	
	$('select.chosen-select').chosen({inherit_select_classes: true});
	$('#AgentBooking  #BookingCountryId').change(function(){
		loadcity($(this).val());
	});
	
	$('#AgentBooking  #BookingCityId').change(function(){
		loadCourse($(this).val());
	});
	
	$('#AgentBooking  #BookingCourseId').change(function(){
		var course_id = $(this).val();
		//loadAddon(course_id);
		loadCourseDate(course_id);
		loadCoursePickuplocation(course_id);
	});
	
	$('#AgentBooking  #BookingDate').change(function(){
		loadCourseTimming($(this).val());
	});
	$('#AgentBooking  #BookingTime').change(function(){
		loadCoursePax($(this).val());
	});
	$('#AgentBooking  #BookingPax').change(function(){
		var course_id = $('#AgentBooking  #BookingCourseId').val();
		loadAddon(course_id);
		calculate_total();
	});
	
	$('#AgentBooking  #BookingAddons').change(function(){
		calculate_total();
		});
	//$("#BookingAddons_chosen").click(function(){
	//	calculate_total();
	//});
	
	$('#AgentBooking').submit(function(){
		$('.error-message').remove();
		var is_valid = 1;
		if($('#AgentBooking #BookingCountryId').val()==''){
			$('#AgentBooking #BookingCountryId_chosen').addClass("invalid form-error").after('<div class="error-message">Please select a country.</div>');
			$('#AgentBooking #BookingCountryId_chosen').unbind('click').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingCityId').val()==''){
			$('#AgentBooking #BookingCityId_chosen').addClass("invalid form-error").after('<div class="error-message">Please select city.</div>');
			$('#AgentBooking #BookingCityId_chosen').unbind('click').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingFirstName').val()==''){
			$('#AgentBooking #BookingFirstName').addClass("invalid form-error").after('<div class="error-message">Please select First Name.</div>');
			$('#AgentBooking #BookingFirstName').unbind('click').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingLastName').val()==''){
			$('#AgentBooking #BookingLastName').addClass("invalid form-error").after('<div class="error-message">Please select Last Name.</div>');
			$('#AgentBooking #BookingLastName').unbind('click').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingEmail').val()==''){
			$('#AgentBooking #BookingEmail').addClass("invalid form-error").after('<div class="error-message">Please select Email.</div>');
			$('#AgentBooking #BookingEmail').unbind('click').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}else{
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	if(!re.test($('#AgentBooking #BookingEmail').val())){
				$('#AgentBooking #BookingEmail').addClass("invalid form-error").after('<div class="error-message">Please enter valid email address.</div>');
				$('#AgentBooking #BookingEmail').unbind('click').bind('click',function(){
					$(this).removeClass('invalid form-error');
					$(this).parent().find('.error-message').remove();
				});
				is_valid = 0;
			}
		}
		
		if($('#AgentBooking #BookingDate').val()==''){
			$('#AgentBooking #BookingDate_chosen').addClass("invalid form-error").after('<div class="error-message">Please select a date.</div>');
			$('#AgentBooking #BookingDate_chosen').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingTime').val()==''){
			$('#AgentBooking #BookingTime_chosen').addClass("invalid form-error").after('<div class="error-message">Please select time.</div>');
			$('#AgentBooking #BookingTime_chosen').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if($('#AgentBooking #BookingPax').val()==''){
			
		$('#AgentBooking #BookingPax_chosen').addClass("invalid form-error").after('<div class="error-message">Please select pax.</div>');
			$('#AgentBooking #BookingPax_chosen').bind('click',function(){
				$(this).removeClass('invalid form-error');
				$(this).parent().find('.error-message').remove();
			});
			is_valid = 0;
		}
		
		if(pickup_location_status!=1){
			if($('#AgentBooking input[name="data[Booking][pickup_address]"]').val()==''){
				$('#AgentBooking input[name="data[Booking][pickup_address]"]').after('<div class="error-message">Please enter pick up address</div>');
				$('#AgentBooking input[name="data[Booking][pickup_address]"]').bind('click',function(){
					$(this).removeClass('invalid form-error');
					$(this).parent().parent().find('.error-message').remove();
				});
				is_valid = 0;
				
			}
			if(!$('#AgentBooking input[name="data[Booking][is_map_correct]"]').is(':checked')){
				$('#AgentBooking #CourseIsMapCorrect').after('<div class="error-message">Please select yes or no</div>');
				$('#AgentBooking input[name="data[Booking][is_map_correct]"]').bind('click',function(){
					$(this).removeClass('invalid form-error');
					$(this).parent().parent().find('.error-message').remove();
				});
				is_valid = 0;
			
			}
		}
		
		if(!is_valid){
			return false;
		}
		return calculate_total();
	});
	
	$('.future-booking-see-more-link a').bind('click',function(){
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadfuturebooking'))?>',
			async: true,
			data: {page:futurebooking_page+1},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					$('.future-booking-loop').append(data.html);
				}
				if(data.next_page){
					futurebooking_page++;
				}else{
					$('.future-booking-see-more-link').hide();
				}
			}
		});
		return false;
	});
	
	$('.past-booking-see-more-link a').bind('click',function(){
		$.ajax({
			url: '<?=Router::url(array('controller'=>'agents','action'=>'loadpastbooking'))?>',
			async: true,
			data: {page:pastbooking_page+1},
			dataType:'json', 
			type:'post',
			
			success: function(data) {
				if(data.status=="success"){
					$('.past-booking-loop').append(data.html);
				}
				if(data.next_page){
					pastbooking_page++;
				}else{
					$('.past-booking-see-more-link').hide();
				}
			}
		});
		return false;
	});
	
	function adjust_action_box(){
		$(".agent-booking-form .action-box").removeAttr("style");
		var booking_form_action_height = $(".agent-booking-form .action-box").height();
		var left_section_offset_top = $('.agent-booking-form .left-sec').offset().top;
		var left_section_height = $('.agent-booking-form .left-sec').height();
		var action_box_offset_top = $(".agent-booking-form .action-box").offset().top;
		var margintop = ((left_section_offset_top + left_section_height) - action_box_offset_top);
		if(margintop < 0){
			$(".agent-booking-form .action-box").css("margin-top",margintop);
	  }else{
			$(".agent-booking-form .action-box").removeAttr("style");
		}
		
		
	}
	
	adjust_action_box();
});
</script>

<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyD-Z5RORk7Oeo2L-kralZOoBaEo9yec2YI&v=3.exp&signed_in=true&libraries=places"></script>
<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
var map = null;
function initialize() {
	
  var markers = [];
  
  var mapOptions = {
			zoom: 15,
            center: new google.maps.LatLng(default_latitude, default_longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
  map = new google.maps.Map(document.getElementById('map-location'), mapOptions/*{
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }*/);  
  
  var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(default_latitude, default_longitude));// new google.maps.LatLng(-33.8902, 151.1759),new google.maps.LatLng(-33.8474, 151.2631)
      
  map.fitBounds(defaultBounds);

  // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pickup_address'));
  ////map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);//We do not want to move our input box

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

  // [START region_getplaces]
  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      $('#pickup_lat').val(place.geometry.location.lat());
      $('#pickup_long').val(place.geometry.location.lng());
      
      var marker = new google.maps.Marker({
        map: map,
        //icon: image,//We don't different image
        draggable:true,
        title: place.name,
        position: place.geometry.location
      });
      
      google.maps.event.addListener(marker, 'dragend', function (event) {
			$('#pickup_lat').val(this.getPosition().lat());
			$('#pickup_long').val(this.getPosition().lng());	
	  });

      markers.push(marker);

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
    map.setZoom(15);
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);


function load_host_pickup_location(lati,longi) {
	//var lati = 24.575351;
	//var longi = 73.68001600000002;
	
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(lati,longi);//-34.397, 150.644);
	var mapOptions = {
		zoom: 15,
		center: latlng
	}
	map = new google.maps.Map(document.getElementById('map-location'), mapOptions);
	
	map.setCenter(latlng);
		var Marker = new google.maps.Marker({
		map: map,
		//draggable:true,
		position: latlng,
	});
		
	is_loaded = 1;
	return true;
}
</script>
