
<h3 class="heading-mt">Forgot Your Password?</h3>

<div class="white-back-full">
  	<?php echo $this->Element('message');?>
  <div class="registration-form-box">
    <?php echo $this->Form->create('Agent', array('class'=>'registration-form','novalidate' => true));?>
      <?=$this->Form->hidden('form-name',array('required'=>false,'value'=>'AgentResetPasswordForm')); ?>
      <div class="registration-form-row">
	<p>Don't worry! Just fill in your email and we'll help you reset your password.</p>
      </div>
      <div class="course-dates inputs inputs-4">
	     <label class="label">Email address: <span style="color:#ff0000">*</span></label>
	      <?=$this->Form->email('email',array('required'=>false)); ?>
	</div>
		<div class="save-btn" style="">
		<input class="submit-button" value="Submit" type="submit"><div id="pre_loader"><?=$this->Html->image('preloader.png');?></div>
	</div>
	<?php echo $this->Form->end();?>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
	<?php $path = $this->Html->webroot; ?>
		$(document).ready(function(){
		$('#AgentResetpasswordForm').submit(function(){
			var data = new FormData(this);
			var formData = $(this);
				var status = 0;
			$.each(this,function(i,v){
					$(v).removeClass('invalid form-error');
					});
				$('.error-message').remove();
				$('#UserResetpasswordForm > span#for_owner_cms').show();
				$('#UserResetpasswordForm > button[type=submit]').attr({'disabled':true});
				$('#pre_loader').show();
			$.ajax({
					url: '<?=$path?>agents/ajax_validation',
					async: false,
					data: data,
					dataType:'json', 
					type:'post',
					cache: false,
					contentType: false,
					processData: false,
					success: function(data) {
						$('#pre_loader').hide();
						if(data.error==1){
							$.each(data.errors,function(i,v){
			    $('#'+i).addClass("invalid form-error").after('<div class="error-message">'+v+'</div>');
				$('#'+i).bind('click',function(){
			    $(this).removeClass('invalid form-error');
			    $(this).next().remove();
			    });
							});
						}else{
							status = 1;
						}
					}
				});
				if(status==0){
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$('#UserResetpasswordForm > button[type=submit]').attr({'disabled':false});
				$('#UserResetpasswordForm > span#for_owner_cms').hide();
				}
			return (status===1)?true:false; 
			});
		});
</script>
