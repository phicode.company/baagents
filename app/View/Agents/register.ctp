<!--Popup for signup -->
<h3 class="heading-mt">Sign Up</h3>
	<div class="white-back-full">
		<div class="fields-page agent-register">
			<div class="">
			<div id="signup">
				<div class="right-popup-content">
					<h4>Agent Portal Sign Up</h4>
					<div class="facebook-btn">
						<!--
						<a href="javascript:void(0)" onclick="authorizeAppInPopup()"><img src="/img/facebook-signup-btn.png" alt="Sign Up with Facebook"></a><div class="or">OR</div>-->
					</div>
					<?php echo $this->Form->create('Agent',array('name'=>'user','id'=>'AgentRegistration' ,'novalidate' => true, 'class'=>''));?>
					<?=$this->Form->hidden('form-name',array('required'=>false,'value'=>'AgentRegistrationForm')); ?>
					<div class="left"><?=$this->Form->text('first_name',array('required'=>false,'placeholder'=>'First Name')); ?><?=$this->Form->error('first_name',null,array('wrap' => 'div', 'class' => 'error-message')); ?></div>
					<div class="right"><?=$this->Form->text('last_name',array('required'=>false,'placeholder'=>'Last Name')); ?><?=$this->Form->error('last_name',null,array('wrap' => 'div', 'class' => 'error-message')); ?></div>
					<?=$this->Form->email('email',array('required'=>false,'placeholder'=>'Email Address')); ?>
					<?=$this->Form->error('email',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
					<?=$this->Form->text('company_name',array('required'=>false,'placeholder'=>'Company')); ?>
					<?=$this->Form->error('company_name',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
					<?=$this->Form->input('country_id',array('required'=>false,'placeholder'=>'Country','options'=>$countries,'empty'=>'Select Country','label'=>false,'class'=>'chosen-select addon2_selectdropdown','div'=>false)); ?>
					<?=$this->Form->error('country_id',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
					<?=$this->Form->input('city_id',array('required'=>false,'placeholder'=>'City','label'=>false,'empty'=>'Select City','class'=>'chosen-select addon2_selectdropdown','options'=>$cities,'div'=>false)); ?>
					<?=$this->Form->error('city_id',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
					<?=$this->Form->text('mobile',array('required'=>false,'placeholder'=>'Mobile','label'=>false)); ?>
					<?=$this->Form->error('mobile',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
					<?=$this->Form->password('password',array('required'=>false,'placeholder'=>'Password')); ?>
					<?=$this->Form->error('password',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					
					<?=$this->Form->password('confirm_password',array('required'=>false,'placeholder'=>'Confirm Password')); ?>
					<?=$this->Form->error('confirm_password',null,array('wrap' => 'div', 'class' => 'error-message')); ?>
					<div class="agent-reg-sign-btn"><input type="submit" value="Sign Up"/><div id="pre_loader"><?=$this->Html->image('preloader.png');?></div></div>
					<?php echo $this->Form->end();?>
				</div>
			</div>    
		</div>
   </div>
   <div class="clear"></div>    
</div>      
<!--end #signup-->
 <script type="text/javascript">
		function loadcity(countryid){
			$('#AgentRegistration #AgentCityId').html('');
			$.ajax({
					url: '<?=Router::url(array('controller'=>'agents','action'=>'loadCity'))?>',
					async: false,
					data: {country_id:countryid},
					dataType:'json', 
					type:'post',
					
					success: function(data) {
						if(data.status=="success"){
							$('#AgentRegistration #AgentCityId').append('<option value="">Select City:</option>');
							$.each(data.results,function(i,v){
								$('#AgentRegistration #AgentCityId').append('<option value="'+i+'">'+v+'</option>');
								});
						}
						$('#AgentCityId').trigger("chosen:updated");
					
					}
				});
		}
	
		$(document).ready(function(){
			$('select.chosen-select').chosen({inherit_select_classes: true});
			$('#AgentRegistration  #AgentCountryId').change(function(){
				loadcity($(this).val());
			});
			
			
			
		$('#AgentRegistration').submit(function(){
			var data = new FormData(this);
			var formData = $(this);
				var status = 0;
			$.each(this,function(i,v){
				$(v).removeClass('invalid form-error');
			});
			$('.error-message').remove();
			$('#AgentRegistration span#for_owner_cms').show();
			$('#AgentRegistration button[type=submit]').attr({'disabled':true});
			$('#pre_loader').show();
			$.ajax({
					url: '<?=Router::url(array('controller'=>'agents','action'=>'ajax_validation'))?>',
					async: false,
					data: data,
					dataType:'json', 
					type:'post',
					cache: false,
					contentType: false,
					processData: false,
					success: function(data) {
						$('#pre_loader').hide();
						if(data.error==1){
							$.each(data.errors,function(i,v){
								$('#'+i).addClass("invalid form-error").after('<div class="error-message">'+v+'</div>');
								$('#'+i).bind('click',function(){
									$(this).removeClass('invalid form-error');
									$(this).next('.error-message').remove();
								});
							});
						}else{
							status = 1;
						}
					
					}
				});
				if(status==0){
					$("html, body").animate({ scrollTop: 0 }, "slow");
					$('#AgentRegistration > button[type=submit]').attr({'disabled':false});
					$('#AgentRegistration > span#for_owner_cms').hide();
				}
			return (status===1)?true:false; 
			});
		});
 </script>

			