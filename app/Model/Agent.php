<?php
class Agent extends AppModel {
	public $name = 'Agent';
	public $hasAndBelongsToMany = array(
        'City' =>
            array(
                'className' => 'City',
                'joinTable' => 'agent_cities',
                'foreignKey' => 'agent_id',
                'associationForeignKey' => 'city_id',
                'unique' => true,
                'conditions' => '',
                'fields' => 'id,name',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'agent_cities'
            )
    );
	
	var $actsAs = array('Multivalidatable');
	public  $validationSets = array(
		'BA-AgentRegistrationSETS' => array(
				'first_name'=> array( 
						array( 
							'rule' =>'notBlank', 
							'message'=> 'Please enter your first name.'
							),
						array(
							'rule' => '/^[A-Za-z ]*$/',
							'message' => 'Please enter first name in alphabet.'
							) 
						),
				'last_name'=>  array( 
						array( 
							'rule' =>'notBlank', 
							'message'=> 'Please enter your last name.'
							),
						array(
							'rule' => '/^[A-Za-z ]*$/',
							'message' => 'Please enter last name in alphabet.'
							)  
						),
				'email'=>  array( 
						'notEmpty'=>array(
							'rule' =>array('notBlank'),
							'message'=> 'Please enter your email address.'
							),
						'email'=>array(
							'rule' =>array('email'),
							'message'=> 'Please enter valid email address.'
							),
						'isUnique'=>array(
							'rule'=>array('isUnique'),
							'message'=>'This email has already been registered with us.'
							)
						),
				'password'=>  array( 
						array( 
							'rule' =>'notBlank', 
							'message'=> 'Please enter password.'
							),
						array( 
							'rule' =>array('minLength',6),
							'message'=> 'Password must be at least 6 characters long.'
							)
						),
				'confirm_password'=>array( 
							array( 
								'rule' =>'notBlank', 
								'message'=> 'Confirm your password here.'
							     ),
							array(
								'rule' => 'checkpasswords',
								'message' => 'Your passwords do not match.'
							)
						),
				'country_id'=> array( 
						array( 
							'rule' =>'notBlank', 
							'message'=> 'Please select country.'
							) 
						),
				'city_id'=> array( 
						array( 
							'rule' =>'notBlank', 
							'message'=> 'Please select city.'
							) 
						),
				'mobile'=> array(
						array(
							'rule' =>array('CheckPhoneNo'),
							'allowEmpty'=>true,							
							'required'=>false,							
							'message'=> 'Phone number(s) must be valid.'
							)
						),
				'post_code'=> array( 
						array( 
							'rule' =>'notBlank', 
							'message'=> 'Please enter post code.'
							) 
						),
				),
		'BA-AgentLoginSETS'=>array(
				'email' =>	array(
						'rule1' =>
							array(
							 'rule' => 'notBlank',
							'message' => 'Please enter email address.'
							),
						array(
						   'rule' => array('email'),
							'message' => 'Please enter email address in a correct format.'
						) 
				),
				'password'=>  array( 
					array( 
					'rule' =>'notBlank', 
					'message'=> 'Please enter password.'
					) 
				),
			),
		'BA-AgentResetPasswordSETS'=>array(
				'email' =>	array(
						'rule1' =>
							array(
							 'rule' => 'notBlank',
							'message' => 'Please enter email address.'
							),
							array(
								 'rule' => array('email'),
								'message' => 'Please enter email address in a correct format.'
							),
						'isexits'=>array(
							'rule'=>array('CheckEmail'),
							'message'=>'This email is not registered with us.'
							)
					),
			
			)
		);
	
	function checkpasswords(){
		if(strcmp($this->data['Agent']['password'],$this->data['Agent']['confirm_password']) == 0 ){
			return true;
		}
		return false;
	}
	
	function CheckPhoneNo(){
		$errors = '';
		$phone = $this->data['Agent']['mobile'];
		if (!preg_match('/^\+?[0-9 \-]+$/', $phone) && $phone!='') {
			$errors [] = 1;
		}
		else if( (strlen($phone) < 8 || strlen($phone) > 15) && $phone!=''){
			$errors [] = 2;
		}
		
		if(!empty($errors)){
			return false;
		}
		return true;
	}
	
	function CheckEmail(){
		$count = $this->find('count',array('conditions'=>array('Agent.email'=>$this->data['Agent']['email'],'Agent.status'=>array(1,2))));
		if($count > 0){
			return true;
		}
		return false;
	}
	
	
	
}

?>
