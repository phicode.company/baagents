$(document).ready(function(){
  
  /*Home page country city suggestion toggle */
  $(document).on('click', function(e) {
    if ( $(e.target).closest('.country-1').length ) {
      $(".toggle-country").show();
    }else if ( ! $(e.target).closest('.toggle-country').length ) {
      $('.toggle-country').hide();
    }
  });
  //END
  
  /*Home page top city search submit*/
  /*
  $('.cityname').click(function(){
    $('#CourseSearch1').val($(this).attr('id'));
    $('#CourseShowForm1').submit();
  });
  */
  //END
  
  if ($(".home-page-video").length > 0) {
      $(window).resize(function(){
        
      var window_width = $(window).width();
      var video_width = window_width + 100;
      var video_height = (816/1450) *video_width; 
      $(".home-page-video iframe").attr("width",video_width);
      $(".home-page-video iframe").attr("height",video_height);
      //$(".home-page-video").css("height",$(window).height()+ 'px');
      
      if (video_height < $(window).height()) {
        //code
        //$('#player1').height();
        $(".home-page-video").css("height",(video_height-2)+ 'px');
        $(".home-page-video").css("min-height","0px");
        
        
      }else{
           $(".home-page-video").css("height",$(window).height()+ 'px');
      }
      
      
      $(".home-page-video").fitVids();
    });
  }
  
 
 
 
 //Home Page About us Toggle
 $(".how-it-works-link , .how-it-works-top-link").click(function(){
  $('html, body').animate({
        scrollTop: $("#about-ba").offset().top
    }, 750);
  return false;
  });  
  
  
  
  //Pick up location image delete event START
  $('.gallery-side > span.action_pickupimage_delete a.delete ').click(function(){
    var course_id = $(this).attr('course-id');
    var img_name = $(this).attr('img_name');
    $.ajax({
      type: "POST",
      url: '/course_manager/courses/delete_coursepickup_image',
      data: 'course_id='+ course_id+'&img_name='+img_name,
      dataType:'json',
      success: function(result){
        if(result.success==1){
          $(".show_pickup_photo .images").remove();
          $("#pickupimagethumb2").attr("src",result.noImg);
          $('.gallery-side > span.action_pickupimage_delete').hide();
          $('.gallery-preview2').parent().addClass('no-pickup-img');
          $('#_pickupphoto').val('');
          //$('#picId_'+Pic_id).remove();
        }else{
          alert(result.fail_message);//show message
        }
    }
    });
  });
  
  
  //Front end Pickup location upload image function START
  if ($('div#gallerydropzone2').length > 0) {
    //code
    var myDropzone = new Dropzone("div#gallerydropzone2", { 
			url: "/course_manager/courses/ajax_upload",
			autoProcessQueue: true,
			uploadMultiple: false,
			parallelUploads: 1,
			acceptedFiles:'image/*',
			maxFiles: 1,
			addRemoveLinks:true,
			files: {},
			init: function() {
				this.on("complete", function(file) {
					/* If file upload success */
					if(file.accepted == true && file.status=='success'){
						//alert('test');
						this.removeFile(file);
					}
				});
			},
			maxfilesexceeded: function(file) {
					this.removeAllFiles();
					this.addFile(file);
			},
			success:function(file,response){
					changes = true;
					response = $.parseJSON( response );
					$.each(response,function(i,v){
						var file_hash = file.name+'-'+file.size; 
						$("#pickupimagethumb2").attr("src",$(v.thumb2).attr("src"));
						$("#_pickupphoto").val(v.name);
						 $('.gallery-side > span.action_pickupimage_delete').show();
						 $('.gallery-side > span.action_pickupimage_delete > a.delete').attr("img_name",v.name);
						 $('.gallery-preview2').parent().removeClass('no-pickup-img');
					});
				},
				sending:function(file, xhr, formData){
					formData.append("action", 'add_new');
				}
			});
  }
  //Front end Pickup location upload image function END
  
 
    
   
}); // document ready ends


$(window).load(function(){
  
  /*No Needs to load for mobile homepage only*/
  //if (!isMobile.any() && window.location.pathname=="/") {
    
    /* <!--Start of Zopim Live Chat Script-->*/
 
    /* <!--End of Zopim Live Chat Script--> */
    
  //}
    
    
    //Home Page Testimonials section set height for desktop and mobile.
    if ($(".home-testimonial").length > 0) {
      var max_height = 0;
      $(".home-testimonial  ul  li").each(function(i,v){
          if (max_height < $(v).height()) {
            max_height = $(v).height();
          }
      });
      $(".home-testimonial .mobile-testimonial-section").css('height', max_height + 'px');
      
      //code
    }
    
    
    /* START HOME PAGE VIDEO*/
    function onPlayProgress(data) {
      //console.log(data);
      if (getCookie('isvideoLoaded')==1) {
        //code
          if(data.seconds > 0){
            jQuery('#homepage-flexslider').hide();
            //jQuery('#player1').show();
          }
      }else{
        if (data.seconds > 0.5) {
          
           var iframe = $('#player1')[0];
           var player = $f(iframe);
           player.api('pause');
           player.api('loop',1);
           player.api('seekTo',0);
           setCookie('isvideoLoaded',1,90);
           //player.api('finish');
           player.api('play');
        }
      }
      
      //status.text(data.seconds + 's played');
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length,c.length);
            }
        }
        return "";
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    } 
    function onFinish() {
        //alert('video has ended');
         // var iframe = $('#player1')[0];
         // var player = $f(iframe);
        // player.api('play');
       // console.log('video has ended');
        //$('#vimeoembed').addClass('finished');
    }
    function loop() {
      var vid =  document.getElementById('vid');
      //console.log(vid);
      var b = vid.buffered,  /// get buffer object
      i = b.length,      /// counter for loop
      //w = canvas.width,  /// cache canvas width and height
      //h = canvas.height,
      vl = vid.duration, /// total video duration in seconds
      x1, x2;            /// buffer segment mark positions
      while (i--) {
        b.start(0);
        b.end(0);
        
        
      console.log(i);  
      console.log('start:'+b.start(0));
      console.log('end: '+b.end(0));
      }
      
      if(!vid.paused){
        ///jQuery('#homepage-flexslider').hide();
      }
      
      if (i == 0) {
        
        if (b.end(0) < (vl-10)) {
          setTimeout(loop, 29);
        }
    
       
      }
        
    }
      var isvideostarted = 0;
     function updateProgressBar() {
        var vid =  document.getElementById('vid');
         //console.log(isvideostarted);
         if (getCookie('isvideoLoadedCustom')==1 || isvideostarted==1) {
         
            /*console.log('Loading: '+vid.currentTime); */
            if(vid.currentTime > 0) {
               jQuery('#homepage-flexslider').hide();
            }
            //code
          }else{
            /*console.log('Playing'); */
            if(vid.currentTime >= (vid.duration-10)) {
              isvideostarted = 1;
              vid.currentTime = 0;
              setCookie('isvideoLoadedCustom',1,90);
            }
            if (vid.buffered.end(0) >= (vid.duration-10)) {
              isvideostarted = 1;
              vid.currentTime = 0;
              setCookie('isvideoLoadedCustom',1,90);
            }
            
          }
      }
    
    
    if ($('#homepage-flexslider').length > 0) {
      //code
      
          var country_id = $('#homepage-flexslider').attr('country-id');
          //$(slider[0])
          $('body').removeClass('loading');
          if (country_id=='' || country_id!='ID') {
            $('.vimeoFrame-1').append('<iframe id="player1" src="//player.vimeo.com/video/227551685?loop=1&api=1&player_id=player1&volume=0&autoplay=0" style="z-index:0;" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            var iframe = $('#player1')[0];
            var player = $f(iframe);
            var status = $('.status');
            var window_width = $(window).width();
            //alert(window_width);
            if(window_width >= 1440){
              window_width +=150;
            }
            
            var video_width = window_width + 100;
            var video_height = (9/16) *video_width;
            
            if(video_height >= 900){
              //video_width +=200;
              video_height = 950;
            }
            
            
            $(".home-page-video iframe").attr("width",video_width);
            $(".home-page-video iframe").attr("height",video_height);
            
           
            
            
            // Listen for messages from the player
            // When the player is ready, add listeners for pause, finish, and playProgress
           
            player.addEvent('ready', function() {
              $(".home-page-video").fitVids();
              
              video_height = $(".home-page-video iframe").height();
              if (video_height < $(window).height()) {
                
                //code
                //$('#player1').height();
               // alert(1);
                $(".home-page-video").css("height",(video_height - 60)+ 'px');
                $(".home-page-video").css("min-height","0px");
                
                
              }else{
                  var diff = 0;
                  if(video_height >= 810){
                    //diff = 40;
                  }
                  //height: 810
                  //alert($(".home-page-video iframe").width());
                   $(".home-page-video").css("height",($(window).height()-diff)+ 'px');
              }
              
              
              
              
              player.api('play');
              player.api('setVolume',0);
              player.addEvent('playProgress', onPlayProgress);
              
              //player.addEvent('finish', onFinish);
             // $("#about-ba").offset().top
             $(window).scroll(function(){
             
                if ($(window).scrollTop() >= $('#about-ba').offset().top) {
                   //player.api('pause');
                }else{
                   //player.api('play');
                }
              
              });
            
              
            });
            
            $( window ).resize(function() {
              console.log('test');
               var window_width = $(window).width();
                var video_width = window_width + 100;
                var video_height = (9/16) *video_width; 
                $(".home-page-video iframe").attr("width",video_width);
                $(".home-page-video iframe").attr("height",video_height);
                
                video_height = $(".home-page-video iframe").height();
                if (video_height < $(window).height()) {
                  //code
                  //$('#player1').height();
                  $(".home-page-video").css("height",(video_height - 60)+ 'px');
                  $(".home-page-video").css("min-height","0px");
                  
                  
                }else{
                     $(".home-page-video").css("height",$(window).height()+ 'px');
                }
            });
            
          }else{
            
            jQuery('.vimeoFrame-1').append('<video id="vid" loop="loop" class="video-playing" preload="auto|metadata"  width="100%"  height="100%" autoplay="autoplay" autoplay><source type="video/mp4" src="/video/5/24sec Indo HP Video - handbraked.mp4"></source><source type="video/webm" src="/video/5/24sec_Indo_HP_Video_-_handbraked.webm"></source></video>');
            var window_width = $(window).width();
            var video_width = window_width + 100;
            var video_height = (816/1450) *video_width; 
            $(".home-page-video iframe").attr("width",video_width);
            $(".home-page-video iframe").attr("height",video_height);
            $(".home-page-video").css("height",$(window).height()+ 'px');
            
            $(".home-page-video").fitVids();
            document.getElementById('vid').play();
            document.getElementById('vid').addEventListener('timeupdate', updateProgressBar, false);
            if (getCookie('customvideostarted')==1) {
              isvideostarted=1;
            }else{
              setCookie('customvideostarted',1,90);
            }
            
            
            
           // loop();
          
          }
          
          
          
         
      
    }
    
    
    /* END HOME PAGE VIDEO*/
    
    /*
    $('.flexslider').css('height',$(window).height() + 'px');
		$('.flexslider').css('overflow','hidden');
    $('#popular-destinations').flexslider({
      animation: 'fade',
      start: function (slider) {
        $('body').removeClass('loading');
      }
    });
    */
    
    


}); // window load ends

   